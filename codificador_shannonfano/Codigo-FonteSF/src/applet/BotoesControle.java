/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */

package applet;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.JButton;

// Classe que cria os bot�es (come�ar, proximo, anterior ) para controlar a simula��o
public class BotoesControle extends Box
{
	protected Presenter presenter;
    protected JButton play;
    protected JButton next;
    protected JButton prev;
    
    public BotoesControle()
    {
        super(0);
        presenter = null;
        play = null;
        next = null;
        prev = null;
        play = new JButton("Come�ar");
        play.setAlignmentX(0.5F);
        play.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae)
            {
                playPause();
            }
            final BotoesControle this$0;
            {
                this$0 = BotoesControle.this;
            }
        }
);
        next = new JButton("Pr�ximo");
        next.setAlignmentX(0.5F);
        next.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae)
            {
                next();
            }
            final BotoesControle this$0;
            {
                this$0 = BotoesControle.this;
            }
        }
);
        prev = new JButton("Anterior");
        prev.setAlignmentX(0.5F);
        prev.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae)
            {
                prev();
            }
            final BotoesControle this$0;
            {
                this$0 = BotoesControle.this;
            }
        }
);
    }

    protected void playPause()
    {
        presenter.playPause();
    }

    protected void next()
    {
        presenter.next();
    }

    protected void prev()
    {
        presenter.prev();
    }
    public JButton getPlayButton()
    {
        return play;
    }
    public JButton getNextButton()
    {
        return next;
    }
    public JButton getPrevButton()
    {
        return prev;
    }
    public void setPresenter(Presenter presenter)
    {
        this.presenter = presenter;
        if(presenter.getMode() == 1)
        {
            initButtons();
            add(Box.createHorizontalGlue());
            add(play);
            add(next);
            add(prev);
            add(Box.createHorizontalGlue());
            revalidate();
        } else
        {
            removeAll();
        }
    }
    protected void initButtons()
    {
        play.setText("Come�ar");
        play.setEnabled(presenter.getMode() != 3);
        next.setEnabled(true);
        prev.setEnabled(false);
    }
}
