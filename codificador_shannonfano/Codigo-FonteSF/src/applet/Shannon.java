/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 */
package applet;

/**
*
* Esta classe contem as opera��es b�sicas da codifica�ao e decodifica�ao de Shannon Fano usando listas
*/
public class Shannon {
   int n;
   public CElemento[] lista;
   public String resultados="";
   public String valor="";
   
   //Construtores
   public Shannon() {
       this.lista =new CElemento[1000];
   }
   public Shannon(int n) {
       this.lista =new CElemento[n];
   }
   //Getters e setters
   public String getResultados() {
       return resultados;
   }
   public String getValor() {
       return valor;
   }
  // Ordena a lista por ordem de frequencia usa o algoritmo de bubble sort
   public void ordenar()
   {
       CElemento temp;
       for(int i=n-1;i>=0;i--)
           for(int j=0;j<i;j++)
               if(lista[j].getRepeticion()<lista[j+1].getRepeticion())
               {
                   temp=lista[j];
                   lista[j]=lista[j+1];
                   lista[j+1]=temp;
               }
   }
   //determina se existe um simbolo na lista
   public boolean existe(char valor)
   {
       int i=0;
       boolean ind=false;
       while(i<n && !ind)
       {
           if(lista[i].getCaracter()==valor)
               ind=true;
           i++;
       }
       return ind;
   }
   //Metodo que gera uma lista dos simbolos com suas frequencias de apari�ao
   public void generarLista(String cadena)
   {
       char aux;
       int rep,k=0,longitud=cadena.length();
       CElemento elemento;
       for(int i=0;i<longitud;i++)//Percorre o texto de entrada
       {
           aux=cadena.charAt(i);
           rep=1;
           for(int j=i+1;j<longitud;j++)//cuenta as frequencias de apari�ao de um simbolo
           {
               if(aux==cadena.charAt(j))
                   rep++;
           }
           if(!existe(aux)) //caso nao esteja processado e inserido na lista
           {
               elemento=new CElemento(rep,aux);
               n++;
               lista[k]=elemento;
               k++;
           }
       }
   }

   // Metodo principal de codifica��o
   public void codificar(int i, int j)
   {
       if (j-i>0) // caso base
       {
           //genResultadoParcial();// mostra o resultado parcial de cada chamada recursiva na interface grafica
           int p=dividir(i,j);// dividir a lista em 2 partes cuja suma seja a mais proxima possivel
           for(int k=i;k<=j;k++)
           {
               if (k<p)// Os simbolos da primeira metade da lista tera codigo 0
                   lista[k].setBits(lista[k].getBits()+"0");
               else // Os simbolos da segunda metade da lista tera codigo 1
                   lista[k].setBits(lista[k].getBits()+"1");
           }
           codificar(i,p-1);// Chamada recursiva para codificar a primeira parte da lista
           codificar(p,j);// Chamada recursiva para codificar a segunda parte da lista
       }
   }
   // suma os elementos de uma lista em un intervalo dado
   public int sumaLista(int ini, int fin)
   {
       int suma=0;
       for(int i=ini;i<=fin;i++)
          suma+=lista[i].getRepeticion();
       return suma;
   }
   // divide uma lista em 2 partes cuja suma seja a mais proxima possivel
   public int dividir(int i, int j)
   {
       int suma=0,k=i;
       while(suma<sumaLista(k+1,j) && k<=j)
       {
           suma=suma+lista[k].getRepeticion();
           k++;
       }
       return k;
   }
   // Calcula os bits necessarios para codificar um texto sem usar compacta��o
   public int bitsNecesarios()
   {
       int mayor=0,aux,total=0;
       for(int i=0;i<n;i++)
       {
           aux=lista[i].getBits().length();
           if(aux>mayor)
               mayor=aux;
       }
       total=sumaLista(0,n-1)*mayor;
       return total;
   }
   // Calcula os bits que foram usados na codifica��o por Shannon Fano
   public int bitsComprimidos()
   {
       int total=0;
       for(int i=0;i<n;i++)
       {
           total+=lista[i].getRepeticion()*lista[i].getBits().length();
       }
       return total;
   }
   // Retorna o codigo binario de um simbolo ja codificado
   public String getCodigo(char x)
   {
       int i=0;
       while(i<n && lista[i].getCaracter()!=x)
       {
           i++;
       }
       return lista[i].getBits();
   }
   // Dado o codigo retorna o simbolo correspondente procurando na tabela o lista gerada pela codifica�ao
   public char getCaracter(String cod)
   {
       int i=0;
       while(i<n && !lista[i].getBits().equals(cod))
       {
           i++;
       }
       if(i<n)
           return lista[i].getCaracter();
       else
           return '$';
   }
   
   //Metodo que permite armazenar or resultados de cada passo na codifica�ao numa variavel global
   public void genResultadoParcial()
   {
       for(int i=0;i<n;i++)
          resultados+=""+lista[i].getCaracter()+"�"+lista[i].getRepeticion()+"�"+lista[i].getBits()+"#";
        resultados+=":";
    }
   //Metodo principal de codifica�ao de shannon fano
   public void codificarShannon(String cadena)
   {
       generarLista(cadena); //1.- Gerar uma lista com as frequencias de apari��o de simbolos
       //genResultadoParcial();//Resultados para mostrar na interface
       ordenar();//2.- Ordenar a lista por ordem de frequencias de maior a menor
       codificar(0,n-1); //3.- chamada ao metodo recursivo de codifica��o
      // genResultadoParcial();//Resultados para mostrar na interface
   }
   // Metodo principal para decodificar um texto
   public String decodificar(String cadena)
   {
       String codigo="",aux="";
       char caracter;
       for(int i=0;i<cadena.length();i++)
       {
           //procura uma sequencia de codigos, se ele existe na tabela de simbolos
           //sera inserido a saida d resultado final
           aux+=cadena.charAt(i);
           caracter=getCaracter(aux);//Procura o codigo na tabela de gerada pela codifica�ao e retorna  o simbolo original
           if(existe(caracter))
           {
               codigo+=caracter;
               valor+=aux+":";
               aux="";
           }
       }
       return codigo;
    }

   // Metodos para testar resultados
   public void mostrar()
   {
       for(int i=0;i<n;i++)
          System.out.println("L["+i+"]=("+lista[i].getCaracter()+","+lista[i].getRepeticion()+","+lista[i].getBits()+")");
   }
   public String mostrarCodificacion(String cadena)
   {
       String codigo="";
       for(int i=0;i<cadena.length();i++)
           codigo+=getCodigo(cadena.charAt(i));
       return codigo;
   }
  /*
   public static void main(String args[])
   {
       Shannon sha=new Shannon();
       String cadena="abcdaaabbccccceeeeeddddaababaadaaababaa";
       //String cadena="EXEMPLO DE SHANNON FANO";
       System.out.println("Cadena: "+cadena);
       sha.codificarShannon(cadena);
       System.out.println("La Cadena codificada es: "+sha.mostrarCodificacion(cadena));
       System.out.println("Los pasos que se siguieron son: "+sha.getResultados());
       System.out.println("Los bits necesarios sin codificar seria: "+sha.bitsNecesarios());
       System.out.println("Los bits comprimidos son: "+sha.bitsComprimidos());
       System.out.println("La cadena descomprimida es: "+sha.decodificar(sha.mostrarCodificacion(cadena)));
   }*/
}