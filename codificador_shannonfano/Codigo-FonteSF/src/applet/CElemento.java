/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 */
package applet;

//Esta classe contem os dados de um simbolo como sua repeti��o e seu c�digo
public class CElemento {
   private char caracter;
   private int repeticion;
   private String bits;
   public CElemento()
   {
       repeticion=0;
       caracter='$';
       bits="";
   }
   public CElemento(int repeticion, char caracter) {
       this.repeticion = repeticion;
       this.caracter = caracter;
       this.bits="";
   }
   public char getCaracter() {
       return caracter;
   }
   public int getRepeticion() {
       return repeticion;
   }
   public void setCaracter(char caracter) {
       this.caracter = caracter;
   }
   public void setRepeticion(int repeticion) {
       this.repeticion = repeticion;
   }
   public String getBits() {
       return bits;
   }
   public void setBits(String bits) {
       this.bits = bits;
   }
}
