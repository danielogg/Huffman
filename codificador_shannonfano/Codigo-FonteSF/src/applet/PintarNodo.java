/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */

package applet;

import java.awt.*;


public class PintarNodo
 implements Drawable
{
 //Classe encarregada de pintar um no da arvore 
 public PintarNodo(Color fill, Color stroke, Shape shape)
 {
     this.fill = null;
     this.stroke = null;
     this.shape = null;
     this.fill = fill;
     this.stroke = stroke;
     this.shape = shape;
 }

 public void drawAndFill(Graphics2D graphics)
 {
     if(fill != null)
     {
         graphics.setColor(fill);
         graphics.fill(shape);
     }
     if(stroke != null)
     {
         graphics.setColor(stroke);
         graphics.draw(shape);
     }
 }

 public void draw(Graphics2D graphics)
 {
     graphics.setColor(stroke);
     graphics.draw(shape);
 }

 public void fill(Graphics2D graphics)
 {
     graphics.setColor(fill);
     graphics.fill(shape);
 }

 private Color fill;
 private Color stroke;
 private Shape shape;
}
