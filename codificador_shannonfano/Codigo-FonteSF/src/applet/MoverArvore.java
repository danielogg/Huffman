/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 */
package applet;

//Classe  encarregada de desenhar uma aresta que une dos nos da arvore 
class MoverArvore
{
 private class CoordNode
 {

     private float x;
     private float y;
     private CoordNode next;
     final MoverArvore this$0;

     public CoordNode(float x, float y)
     {
         this$0 = MoverArvore.this;
         this.x = 0.0F;
         this.y = 0.0F;
         next = null;
         this.x = x;
         this.y = y;
     }
 }


 public MoverArvore(TNodo source, TNodo target, int steps)
 {
     diffList = null;
     this.source = null;
     this.target = null;
     this.steps = 0;
     cursor = null;
     this.source = source.copy();
     this.target = target;
     this.steps = steps;
     this.source.clearFlags(true);
     calculateDifference(this.source, this.target);
 }

 public TNodo animate(int step)
     throws IllegalArgumentException
 {
     if(step < 0 && step > steps)
     {
         throw new IllegalArgumentException();
     } else
     {
         TNodo tree = source.copy();
         double factor = (double)step / (double)steps;
         cursor = diffList;
         setNewPosition(tree, factor);
         return tree;
     }
 }

 public TNodo getTarget()
 {
     return target;
 }

 public int getSteps()
 {
     return steps;
 }

 private void setNewPosition(TNodo node, double factor)
 {
     node.moveX((float)((double)cursor.x * factor));
     node.moveY((float)((double)cursor.y * factor));
     cursor = cursor.next;
     if(!node.isLeaf())
     {
         setNewPosition(node.getRightChild(), factor);
         setNewPosition(node.getLeftChild(), factor);
     }
 }

 private void calculateDifference(TNodo source, TNodo target)
     throws NullPointerException
 {
     if(source == null || target == null)
         throw new NullPointerException();
     if(source.getContent().equals(target.getContent()))
     {
         source.setHighlighted(target.isHighlighted());
         source.setSelected(target.isSelected());
         if(!source.isLeaf())
         {
             calculateDifference(source.getLeftChild(), target.getLeftChild());
             calculateDifference(source.getRightChild(), target.getRightChild());
         }
         CoordNode cnode = new CoordNode(target.getX() - source.getX(), target.getY() - source.getY());
         cnode.next = diffList;
         diffList = cnode;
     }
 }

 private CoordNode diffList;
 private TNodo source;
 private TNodo target;
 private int steps;
 private CoordNode cursor;
}
