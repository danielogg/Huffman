/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */

package applet;


import javax.swing.table.AbstractTableModel;
// Classe que desenha a tabela de frequencias
public class TabelaSimbolos extends AbstractTableModel
{
    public TabelaSimbolos(Caracter characters[])
    {
        chars = null;
        frequencies = null;
        codes = null;
        fillorder = null;
        fillorderIndex = -1;
        chars = new String[characters.length];
        frequencies = new int[characters.length];
        for(int i = 0; i < characters.length; i++)
        {
            chars[i] = String.valueOf(characters[i].getChar());
            frequencies[i] = characters[i].getFrequency();
        }

        codes = new String[chars.length];
        fillorder = new int[chars.length];
    }

    public TabelaSimbolos(String chars[], int frequencies[])
    {
        this.chars = null;
        this.frequencies = null;
        codes = null;
        fillorder = null;
        fillorderIndex = -1;
        this.chars = new String[chars.length];
        this.frequencies = new int[frequencies.length];
        codes = new String[chars.length];
        fillorder = new int[chars.length];
        System.arraycopy(chars, 0, this.chars, 0, chars.length);
        System.arraycopy(frequencies, 0, this.frequencies, 0, frequencies.length);
    }

    public void setCode(String character, String code)
    {
        for(int i = 0; i < chars.length; i++)
        {
            if(chars[i].compareTo(character) != 0)
                continue;
            codes[i] = code;
            fillorder[++fillorderIndex] = i;
            break;
        }

    }

    public void removeCode()
    {
        if(fillorderIndex > -1)
            codes[fillorder[fillorderIndex--]] = "";
    }

    public void removeAllCodes()
    {
        codes = new String[chars.length];
        fillorder = new int[chars.length];
        fillorderIndex = -1;
    }

    public int getRowCount()
    {
        return chars.length;
    }

    public int getColumnCount()
    {
        return 3;
    }

    public String getColumnName(int col)
    {
        switch(col)
        {
        case 0: // '\0'
            return "S�mbolo";

        case 1: // '\001'
            return "Freq��ncia";

        case 2: // '\002'
            return "C�digo";
        }
        return "Outros";
    }

    public boolean isCellEditable(int row, int col)
    {
        return false;
    }

    public Object getValueAt(int row, int col)
    {
        switch(col)
        {
        case 0: // '\0'
            return chars[row];

        case 1: // '\001'
            return Integer.valueOf(frequencies[row]);

        case 2: // '\002'
            return codes[row];
        }
        return null;
    }

    /*public Class getColumnClass(int col)
    {
    	
        switch(col)
        {
        case 0: // '\0'
            return java/lang/String;

        case 1: // '\001'
            return java/lang/Integer;

        case 2: // '\002'
            return java/lang/String;
        }
        return java/lang/Object;
        
        
    }*/

    private String chars[];
    private int frequencies[];
    private String codes[];
    private int fillorder[];
    private int fillorderIndex;
}
