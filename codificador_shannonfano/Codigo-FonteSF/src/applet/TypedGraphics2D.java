/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */
package applet;

import java.awt.Graphics2D;

public class TypedGraphics2D
{

    public TypedGraphics2D(Graphics2D graphic, int type)
    {
        this.type = 0;
        this.graphic = null;
        this.graphic = graphic;
        this.type = type;
    }

    public Graphics2D getGraphics()
    {
        return graphic;
    }

    public int getType()
    {
        return type;
    }

    protected int type;
    protected Graphics2D graphic;
}
