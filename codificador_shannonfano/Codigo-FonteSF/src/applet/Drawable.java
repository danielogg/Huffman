/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */

package applet;

import java.awt.Graphics2D;

public interface Drawable
{
    public abstract void drawAndFill(Graphics2D graphics2d);
    public abstract void draw(Graphics2D graphics2d);
    public abstract void fill(Graphics2D graphics2d);
}
