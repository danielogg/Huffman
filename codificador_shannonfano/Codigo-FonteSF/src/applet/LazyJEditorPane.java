/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */
package applet;

import javax.swing.JEditorPane;

public class LazyJEditorPane extends JEditorPane
{

    public LazyJEditorPane(String type, String content)
    {
        super(type, content);
        text = null;
        text = content;
    }

    public void setText(String text)
    {
        if(text == null || this.text == null || this.text != null && this.text.compareTo(text) != 0)
        {
            this.text = text;
            super.setText(text);
        }
    }
    public void setHtmlText(String text)
    {
        setText((new StringBuilder("<html>")).append(text).append("</html>").toString());
    }

    private String text;
}
