/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */
package applet;

import java.awt.*;

public class Utilities
{
    public Utilities()
    {
    }
    public static void drawNode(Graphics2D g, float x, float y, String content, int frequency, boolean leaf, Color color)
    {
        java.awt.geom.Ellipse2D.Float shape = new java.awt.geom.Ellipse2D.Float(x + 15F, y + 15F, 50F, 50F);
        g.setColor(color);
        g.fill(shape);
        g.setColor(Color.BLACK);
        g.draw(shape);
        if(leaf)
        {
            float tmpX = ((x + 40F) - (float)(METRICS_CONTENT.stringWidth(content) / 2)) + 1.0F;
            g.setFont(FONT_CONTENT);
            g.setColor(Color.BLACK);
            g.drawString(content, tmpX, y + 40F);
            String str = (new StringBuilder("h=")).append(frequency).toString();
            tmpX = ((x + 40F) - (float)(METRICS_FREQUENCY.stringWidth(str) / 2)) + 1.0F;
            g.setFont(FONT_FREQUENCY);
            g.drawString(str, tmpX, y + 40F + (float)SIZE_FREQUENCY * 1.2F);
        } else
        {
            String str = (new StringBuilder("h=")).append(frequency).toString();
            float tmpX = ((x + 40F) - (float)(METRICS_FREQUENCY.stringWidth(str) / 2)) + 1.0F;
            g.setFont(FONT_FREQUENCY);
            g.setColor(Color.BLACK);
            g.drawString(str, tmpX, y + 40F + (float)(METRICS_FREQUENCY.getMaxAscent() / 2));
        }
    }

    public static void drawLine(Graphics2D g, float x1, float y1, float x2, float y2, boolean left, boolean highlighted)
    {
        java.awt.geom.Line2D.Float line = new java.awt.geom.Line2D.Float(x1, y1, x2, y2);
        float m = (line.y2 - line.y1) / (line.x2 - line.x1);
        float descrY = line.y1 + (float)SIZE_EDGE + 25F + 5F;
        float descrX = ((descrY - line.y1) / m + line.x1) - 10F;
        g.setFont(FONT_EDGE);
        if(highlighted)
        {
            g.setColor(COLOR_HIGHLIGHT);
            g.draw(line);
        } else
        {
            g.setColor(COLOR_LINE);
            g.draw(line);
            if(left)
                g.setColor(COLOR_EDGE_LEFT);
            else
                g.setColor(COLOR_EDGE_RIGHT);
        }
        g.drawString(left ? "0" : "1", descrX + (float)(left ? -5 : 15), descrY);
    }

    public static java.awt.geom.Line2D.Float calcLine(float x1, float y1, float x2, float y2, double factor)
    {
        float m = (y2 - y1) / (x2 - x1);
        float x = (float)((double)x1 + (double)(x2 - x1) * factor);
        float y = m * (x - x1) + y1;
        return new java.awt.geom.Line2D.Float(x1, y1, x, y);
    }

    public static final Color COLOR_HIGHLIGHT = new Color(187, 64, 255);
    public static final Color COLOR_LEAF = new Color(240, 240, 240);
    public static final Color COLOR_NODE = new Color(200, 200, 200);
    public static final Color COLOR_SELECT = new Color(255, 112, 0);
    public static final Color COLOR_LINE;
    public static final Color COLOR_EDGE_RIGHT = new Color(250, 0, 0);
    public static final Color COLOR_EDGE_LEFT = new Color(0, 150, 0);
    public static final float NODESPACE = 15F;
    public static final float ALPHASPACE = 10F;
    public static final float NODERADIUS = 25F;
    public static final float NODEDIAMETER = 50F;
    public static final float OFFSET_CENTER = 40F;
    public static int SIZE_FREQUENCY;
    public static int SIZE_EDGE;
    public static int SIZE_CONTENT;
    public static final Font FONT_FREQUENCY;
    public static final Font FONT_EDGE;
    public static final Font FONT_CONTENT;
    public static FontMetrics METRICS_FREQUENCY = null;
    public static FontMetrics METRICS_CONTENT = null;

    static 
    {
        COLOR_LINE = Color.BLACK;
        SIZE_FREQUENCY = 10;
        SIZE_EDGE = 15;
        SIZE_CONTENT = 20;
        FONT_FREQUENCY = new Font("Arial", 0, SIZE_FREQUENCY);
        FONT_EDGE = new Font("Arial", 0, SIZE_EDGE);
        FONT_CONTENT = new Font("Arial", 0, SIZE_CONTENT);
    }
}
