/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */
package applet;

import java.awt.*;
import javax.swing.JPanel;


public class PaintCanvas extends JPanel
{

    public PaintCanvas(int type)
    {
        client = null;
        this.type = 0;
        setSize(300, 300);
        setBackground(Color.WHITE);
        this.type = type;
    }

    public void setClient(PaintCanvasClient client)
    {
        this.client = client;
        revalidate();
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        g2d.setRenderingHints(new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON));
        if(client != null)
            client.paint(new TypedGraphics2D(g2d, type));
    }

    protected PaintCanvasClient client;
    protected int type;
}
