/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */
package applet;

public interface Presenter
{
    public abstract void playPause();
    public abstract void start();
    public abstract void next();
    public abstract void prev();
    public abstract void kill();
    public abstract void show();
    public abstract int getMode();
    public abstract boolean isAlive();
    public abstract void updateTarget();
    public static final int MODE_LEARN = 1;
    public static final int MODE_TRAIN = 2;
    public static final int MODE_COMPARE = 3;
}