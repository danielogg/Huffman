/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */

package applet;


import java.awt.Dimension;
import java.awt.Graphics2D;

class TNodo
    implements Cloneable
{

    public TNodo(Simbolo content)
    {
        parent = null;
        left = null;
        right = null;
        this.content = null;
        code = null;
        selected = false;
        x = 0.0F;
        y = 0.0F;
        distance = 0;
        leafs = 0;
        highlighted = false;
        dimension = null;
        alphaDimension = null;
        sfLeafs = null;
        this.content = content;
        if(isSfLeaf())
            sfLeafs = (new TNodo[] {
                this
            });
        else
            sfLeafs = new TNodo[0];
    }

    public Dimension getDimension()
    {
        if(dimension == null)
            dimension = new Dimension(leafs * 40 * 2, (distance + 1) * 40 * 2);
        return dimension;
    }

    public Dimension getAlphaDimension()
    {
        if(alphaDimension == null)
            alphaDimension = new Dimension(content.getCharacterCount() * 40 * 2, 80);
        return alphaDimension;
    }

    public TNodo contains(int posX, int posY)
    {
        TNodo retval = null;
        float xm = x + 40F;
        float ym = y + 40F;
        double distance = Math.sqrt(Math.pow((float)posX - xm, 2D) + Math.pow((float)posY - ym, 2D));
        if(distance <= 25D)
            retval = this;
        else
        if(!isLeaf())
        {
            retval = left.contains(posX, posY);
            if(retval == null)
                retval = right.contains(posX, posY);
        }
        return retval;
    }

    public Object clone()
    {
        return copy();
    }

    public TNodo copy()
    {
        TNodo retval = new TNodo(new Simbolo(content));
        if(!isLeaf())
        {
            retval.left = left.copy();
            retval.left.parent = retval;
            retval.right = right.copy();
            retval.right.parent = retval;
        }
        if(isSfLeaf())
            retval.sfLeafs = (new TNodo[] {
                retval
            });
        else
        if(retval.left != null && retval.right != null)
        {
            retval.sfLeafs = new TNodo[retval.left.sfLeafs.length + retval.right.sfLeafs.length];
            System.arraycopy(retval.left.sfLeafs, 0, retval.sfLeafs, 0, retval.left.sfLeafs.length);
            System.arraycopy(retval.right.sfLeafs, 0, retval.sfLeafs, retval.left.sfLeafs.length, retval.right.sfLeafs.length);
        } else
        if(retval.left != null)
        {
            retval.sfLeafs = new TNodo[retval.left.sfLeafs.length];
            System.arraycopy(retval.left.sfLeafs, 0, retval.sfLeafs, 0, retval.left.sfLeafs.length);
        } else
        if(retval.right != null)
        {
            retval.sfLeafs = new TNodo[retval.right.sfLeafs.length];
            System.arraycopy(retval.right.sfLeafs, 0, retval.sfLeafs, 0, retval.right.sfLeafs.length);
        }
        retval.x = x;
        retval.y = y;
        retval.highlighted = highlighted;
        retval.selected = selected;
        retval.code = code;
        retval.distance = distance;
        retval.leafs = leafs;
        return retval;
    }

    public void setChildren(TNodo left, TNodo right)
    {
        if(left == null || right == null)
        {
            throw new IllegalArgumentException("Children may not be null.");
        } else
        {
            this.left = left;
            this.right = right;
            this.left.parent = this;
            this.right.parent = this;
            updateTree();
            return;
        }
    }

    private void updateTree()
    {
        dimension = null;
        alphaDimension = null;
        distance = (left.distance <= right.distance ? right.distance : left.distance) + 1;
        if(left != null && right != null)
        {
            sfLeafs = new TNodo[left.sfLeafs.length + right.sfLeafs.length];
            System.arraycopy(left.sfLeafs, 0, sfLeafs, 0, left.sfLeafs.length);
            System.arraycopy(right.sfLeafs, 0, sfLeafs, left.sfLeafs.length, right.sfLeafs.length);
        } else
        if(left != null)
        {
            sfLeafs = new TNodo[left.sfLeafs.length];
            System.arraycopy(left.sfLeafs, 0, sfLeafs, 0, left.sfLeafs.length);
        } else
        if(right != null)
        {
            sfLeafs = new TNodo[right.sfLeafs.length];
            System.arraycopy(right.sfLeafs, 0, sfLeafs, 0, right.sfLeafs.length);
        }
        if(parent != null)
            parent.updateTree();
        else
            reposition(0, 0);
    }

    private void reposition(int leafsOnLeft, int leafsOnTop)
    {
        leafs = 0;
        if(!isLeaf())
        {
            left.reposition(leafsOnLeft, leafsOnTop + 1);
            leafs += left.leafs;
            right.reposition(leafsOnLeft + leafs, leafsOnTop + 1);
            leafs += right.leafs;
        }
        x = (float)leafsOnLeft * 40F * 2.0F + ((float)leafs * 40F) / 2.0F;
        y = (float)leafsOnTop * 40F * 2.0F;
        if(isLeaf())
            leafs++;
    }

    public void clearFlags(boolean ignoreLeafs)
    {
        if(!isLeaf())
        {
            highlighted = false;
            selected = false;
            left.clearFlags(ignoreLeafs);
            right.clearFlags(ignoreLeafs);
        } else
        if(!ignoreLeafs)
        {
            highlighted = false;
            selected = false;
        }
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setHighlighted(boolean highlighted)
    {
        this.highlighted = highlighted;
    }

    public boolean isHighlighted()
    {
        return highlighted;
    }

    public void highlightPathTo()
    {
        highlighted = true;
        if(parent != null)
            parent.highlightPathTo();
    }

    public void encodePathTo()
    {
        if(parent != null)
        {
            if(parent.code == null)
                parent.encodePathTo();
            if(parent.left == this)
                code = (new StringBuilder(String.valueOf(parent.code))).append("0").toString();
            else
            if(parent.right == this)
                code = (new StringBuilder(String.valueOf(parent.code))).append("1").toString();
        } else
        {
            code = "";
        }
    }

    public boolean isLeaf()
    {
        return left == right && right == null;
    }

    public boolean isSfLeaf()
    {
        return getContent().getCharacterCount() == 1;
    }

    public Simbolo getContent()
    {
        return content;
    }

    public String getCode()
    {
        return code;
    }

    public int getHeight()
    {
        return distance;
    }

    public int getFrequency()
    {
        return content.getFrequency();
    }

    public TNodo getParent()
    {
        return parent;
    }

    public TNodo getLeftChild()
    {
        return left;
    }

    public TNodo getRightChild()
    {
        return right;
    }

    public TNodo[] getSfLeafs()
    {
        return sfLeafs;
    }

    public float getX()
    {
        return x;
    }

    public void moveX(float x)
    {
        this.x += x;
    }

    public void moveY(float y)
    {
        this.y += y;
    }

    public float getY()
    {
        return y;
    }

    public TNodo getFirstSelectedNode()
    {
        TNodo retval = null;
        if(selected)
            retval = this;
        else
        if(!isLeaf())
        {
            retval = left.getFirstSelectedNode();
            if(retval == null)
                retval = right.getFirstSelectedNode();
        }
        return retval;
    }

    public void draw(Graphics2D g)
    {
        java.awt.Color color = null;
        if(!isLeaf())
        {
            Utilities.drawLine(g, x + 40F, y + 40F, left.x + 40F, left.y + 40F, true, highlighted && left.highlighted);
            Utilities.drawLine(g, x + 40F, y + 40F, right.x + 40F, right.y + 40F, false, highlighted && right.highlighted);
        }
        if(!isSfLeaf())
            color = Utilities.COLOR_NODE;
        else
            color = Utilities.COLOR_LEAF;
        if(highlighted)
            color = Utilities.COLOR_HIGHLIGHT;
        else
        if(selected)
            color = Utilities.COLOR_SELECT;
        Utilities.drawNode(g, x, y, String.valueOf(content.getFirst().getChar()), content.getFrequency(), isSfLeaf(), color);
        if(!isLeaf())
        {
            left.draw(g);
            right.draw(g);
        }
    }

    private TNodo parent;
    private TNodo left;
    private TNodo right;
    private Simbolo content;
    private String code;
    private boolean selected;
    private float x;
    private float y;
    private int distance;
    private int leafs;
    private boolean highlighted;
    private Dimension dimension;
    private Dimension alphaDimension;
    private TNodo sfLeafs[];
}
