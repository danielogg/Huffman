/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */

package applet;

import java.awt.Graphics2D;



// Casse 
public class Simbolo
{
    protected class Node
    {

        protected Caracter content;
        protected Node next;
        final Simbolo this$0;

        public Node(Caracter content)
        {
            this$0 = Simbolo.this;
            this.content = null;
            next = null;
            this.content = content;
        }

        public Node(Caracter content, Node next)
        {
            this$0 = Simbolo.this;
            this.content = null;
            this.next = null;
            this.content = content;
            this.next = next;
        }
    }


    public Simbolo()
    {
        first = null;
        frequency = 0;
        charCount = 0;
        interactiveBoxesLeftChars = 0;
    }

    public Simbolo(String text)
    {
    	class _cls1CountlistNode
        {

            public char content;
            public int amount;
            public _cls1CountlistNode next;
            final Simbolo this$0;

            public _cls1CountlistNode(char content)
            {
                this$0 = Simbolo.this;
                amount = 1;
                next = null;
                this.content = content;
            }
        }
    	
        first = null;
        frequency = 0;
        charCount = 0;
        interactiveBoxesLeftChars = 0;
        int charAmount = text.length();
        char textAsChar[] = new char[charAmount];
        text.getChars(0, charAmount, textAsChar, 0);
        _cls1CountlistNode countlistFirst = null;
        

        for(int i = 0; i < textAsChar.length; i++)
            if(countlistFirst == null)
            {
                countlistFirst = new _cls1CountlistNode(textAsChar[i]);
            } else
            {
                for(_cls1CountlistNode cursor = countlistFirst; cursor != null; cursor = cursor.next)
                {
                    if(cursor.content == textAsChar[i])
                    {
                        cursor.amount++;
                        break;
                    }
                    if(cursor.next != null)
                        continue;
                    cursor.next = new _cls1CountlistNode(textAsChar[i]);
                    break;
                }

            }

        for(_cls1CountlistNode cursor = countlistFirst; cursor != null; cursor = cursor.next)
            add(new Caracter(cursor.content, cursor.amount));

    }

    public Simbolo(Simbolo source)
    {
        first = null;
        frequency = 0;
        charCount = 0;
        interactiveBoxesLeftChars = 0;
        frequency = source.frequency;
        charCount = source.charCount;
        interactiveBoxesLeftChars = source.interactiveBoxesLeftChars;
        if(source.first != null)
        {
            first = new Node(new Caracter(source.first.content));
            Node targetcursor = first;
            for(Node sourcecursor = source.first.next; sourcecursor != null; sourcecursor = sourcecursor.next)
                targetcursor = targetcursor.next = new Node(new Caracter(sourcecursor.content));

        }
    }

    public void add(Caracter character)
    {
        if(first == null)
            first = new Node(character);
        else
        if(first.content.getFrequency() < character.getFrequency())
        {
            first = new Node(character, first);
        } else
        {
            for(Node cursor = first; cursor != null; cursor = cursor.next)
            {
                if(cursor.next == null)
                {
                    cursor.next = new Node(character);
                    break;
                }
                if(cursor.content.getFrequency() < character.getFrequency() || cursor.next.content.getFrequency() >= character.getFrequency())
                    continue;
                cursor.next = new Node(character, cursor.next);
                break;
            }

        }
        frequency += character.getFrequency();
        charCount++;
    }

    public int getCharacterCount()
    {
        return charCount;
    }

    public Caracter[] getCharacterArray()
    {
        Caracter result[] = new Caracter[charCount];
        int i = 0;
        for(Node cursor = first; cursor != null;)
        {
            result[i] = cursor.content;
            cursor = cursor.next;
            i++;
        }

        return result;
    }

    public Simbolo[] split()
    {
        int alphaSizes[] = splitSizeOnly();
        if(alphaSizes == null)
            return null;
        Simbolo result[] = {
            new Simbolo(), new Simbolo()
        };
        Node cursor = first;
        for(int i = 0; i < alphaSizes.length; i++)
        {
            for(int j = 0; j < alphaSizes[i]; j++)
            {
                result[i].add(cursor.content);
                cursor = cursor.next;
            }

        }

        return result;
    }

    public Simbolo[] splitAt(int leftChars)
    {
        Simbolo result[] = {
            new Simbolo(), new Simbolo()
        };
        Node cursor = first;
        for(int i = 0; i < leftChars; i++)
        {
            result[0].add(cursor.content);
            cursor = cursor.next;
        }

        for(int i = leftChars; i < charCount; i++)
        {
            result[1].add(cursor.content);
            cursor = cursor.next;
        }

        return result;
    }

    public int[] splitSizeOnly()
    {
        if(charCount < 2)
            return null;
        int retval[] = {
            1, 0
        };
        double middle = frequency / 2;
        Node end = first;
        int freq = first.content.getFrequency();
        while(end.next != null) 
        {
            if(Math.abs(middle - (double)freq) < Math.abs(middle - (double)(freq + end.next.content.getFrequency())))
                break;
            freq += end.next.content.getFrequency();
            end = end.next;
            retval[0]++;
        }
        retval[1] = charCount - retval[0];
        return retval;
    }

    public Caracter getFirst()
    {
        if(first != null)
            return first.content;
        else
            return null;
    }

    public String toString()
    {
        String result = new String();
        for(Node cursor = first; cursor != null; cursor = cursor.next)
            result = (new StringBuilder(String.valueOf(result))).append(cursor.content).append("\n").toString();

        result = (new StringBuilder(String.valueOf(result))).append(" frequency: ").append(frequency).toString();
        return result;
    }

    public String getString()
    {
        StringBuilder sb = new StringBuilder();
        for(Node cursor = first; cursor != null; cursor = cursor.next)
            sb.append(cursor.content.getChar());

        return sb.toString();
    }

    public boolean equals(Object obj)
    {
        if(obj instanceof Simbolo)
            return equals((Simbolo)obj);
        else
            return false;
    }

    public boolean equals(Simbolo rhs)
    {
        if(rhs.charCount == charCount)
        {
            Node lhsCursor = first;
            for(Node rhsCursor = rhs.first; lhsCursor != null && rhsCursor != null; rhsCursor = rhsCursor.next)
            {
                if(!lhsCursor.content.equals(rhsCursor.content))
                    return false;
                lhsCursor = lhsCursor.next;
            }

            return true;
        } else
        {
            return false;
        }
    }

    protected void createFontMetrics(Graphics2D g)
    {
        if(Utilities.METRICS_CONTENT == null)
        {
            g.setFont(Utilities.FONT_CONTENT);
            Utilities.METRICS_CONTENT = g.getFontMetrics();
            g.setFont(Utilities.FONT_FREQUENCY);
            Utilities.METRICS_FREQUENCY = g.getFontMetrics();
        }
    }

    public void draw(Graphics2D g, boolean interactive)
    {
        createFontMetrics(g);
        Simbolo alphas[] = split();
        if(alphas == null)
            return;
        drawChars(g);
        if(interactive)
            drawBoxes(g, interactiveBoxesLeftChars, getFrequencySum(interactiveBoxesLeftChars));
        else
            drawBoxes(g, alphas[0].charCount, alphas[0].frequency);
    }

    public void setInteractiveBoxesLeftChars(int chars)
    {
        interactiveBoxesLeftChars = chars;
    }

    public int getInteractiveBoxesLeftChars()
    {
        return interactiveBoxesLeftChars;
    }

    public int getFrequencySum(int n)
    {
        int i = 0;
        int freq = 0;
        for(Node cursor = first; cursor != null && i < n; i++)
        {
            freq += cursor.content.getFrequency();
            cursor = cursor.next;
        }

        return freq;
    }

    public int getFrequency()
    {
        return frequency;
    }

    public void drawBoxes(Graphics2D g, int chars, int freq)
    {
        if(chars < 1 || chars >= charCount)
        {
            return;
        } else
        {
            java.awt.geom.RoundRectangle2D.Float box1 = new java.awt.geom.RoundRectangle2D.Float(5F, 5F, ((float)chars * 60F + 5F) - 2.0F, 75F, 10F, 10F);
            java.awt.geom.RoundRectangle2D.Float box2 = new java.awt.geom.RoundRectangle2D.Float((float)chars * 60F + 10F + 2.0F, 5F, (float)(charCount - chars) * 60F + 5F, 75F, 10F, 10F);
            g.setColor(Utilities.COLOR_EDGE_LEFT);
            g.setFont(Utilities.FONT_FREQUENCY);
            g.draw(box1);
            String str = (new StringBuilder("h=")).append(freq).toString();
            float x = ((float)chars * 60F + 5F) - 2.0F - (float)Utilities.METRICS_FREQUENCY.stringWidth(str);
            g.drawString(str, x, 7 + Utilities.SIZE_FREQUENCY);
            g.setColor(Utilities.COLOR_EDGE_RIGHT);
            g.draw(box2);
            str = (new StringBuilder("h=")).append(frequency - freq).toString();
            x = (float)chars * 60F + 15F + 2.0F;
            g.setFont(Utilities.FONT_FREQUENCY);
            g.drawString(str, x, 7 + Utilities.SIZE_FREQUENCY);
            return;
        }
    }

    public void drawChars(Graphics2D g)
    {
        createFontMetrics(g);
        int i = 0;
        float y = 5F;
        for(Node cursor = first; cursor != null;)
        {
            float x = (float)i * 60F;
            Utilities.drawNode(g, x, y, String.valueOf(cursor.content.getChar()), cursor.content.getFrequency(), true, Utilities.COLOR_LEAF);
            cursor = cursor.next;
            i++;
        }

    }

    protected Node first;
    protected int frequency;
    protected int charCount;
    protected int interactiveBoxesLeftChars;
    public static final float BOXHEIGHT = 75F;
}
