/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */
package applet;

import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


public class Codepanels extends JPanel
{

    public Codepanels(int type)
    {
        treecanvas = null;
        alphabetcanvas = null;
        treescroll = null;
        alphabetscroll = null;
        mode = type;
        treecanvas = new PaintCanvas(3);
        treescroll = new JScrollPane(treecanvas);
        setLayout(null);
        add(treescroll);
        if(mode == 2)
        {
            alphabetcanvas = new PaintCanvas(4);
            alphabetscroll = new JScrollPane(alphabetcanvas);
            alphabetscroll.setPreferredSize(new Dimension(0x989680, 100));
            add(alphabetscroll);
        }
        addComponentListener(new ComponentAdapter() {

            public void componentResized(ComponentEvent e)
            {
                resized();
            }

            final Codepanels this$0;

            
            {
                this$0 = Codepanels.this;
 //               super();
            }
        }
);
    }

    protected void resized()
    {
        Dimension panelsize = getSize();
        int alphaHeight = 0;
        if(mode == 2)
            alphaHeight = 120;
        treescroll.setSize(panelsize.width - 20, panelsize.height - alphaHeight - 20);
        treescroll.setLocation(10, 10);
        treescroll.revalidate();
        if(mode == 2)
        {
            alphabetscroll.setSize(panelsize.width - 20, alphaHeight - 10);
            alphabetscroll.setLocation(10, panelsize.height - alphaHeight);
            alphabetscroll.revalidate();
        }
    }

    public PaintCanvas getTreeCanvas()
    {
        return treecanvas;
    }

    public PaintCanvas getAlphabetCanvas()
    {
        return alphabetcanvas;
    }

    public static final int MODE_TREEONLY = 1;
    public static final int MODE_ALPHATREE = 2;
    public static final int TYPE_TREECANVAS = 3;
    public static final int TYPE_ALPHACANVAS = 4;
    protected int mode;
    protected PaintCanvas treecanvas;
    protected PaintCanvas alphabetcanvas;
    protected JScrollPane treescroll;
    protected JScrollPane alphabetscroll;
    protected static final int ALPHAHEIGHT = 120;
}
