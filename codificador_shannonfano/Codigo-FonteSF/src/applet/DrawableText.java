/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */

package applet;

import java.awt.*;

public class DrawableText
 implements Drawable
{

 public DrawableText(String text, Font font, Color color, float x, float y)
 {
     this.text = null;
     this.font = null;
     this.color = null;
     this.x = 0.0F;
     this.y = 0.0F;
     this.text = text;
     this.font = font;
     this.color = color;
     this.x = x;
     this.y = y;
 }

 public void draw(Graphics2D g)
 {
     g.setColor(color);
     g.setFont(font);
     g.drawString(text, x, y);
 }

 public void fill(Graphics2D g)
 {
     draw(g);
 }

 public void drawAndFill(Graphics2D g)
 {
     draw(g);
 }

 private String text;
 private Font font;
 private Color color;
 private float x;
 private float y;
}
