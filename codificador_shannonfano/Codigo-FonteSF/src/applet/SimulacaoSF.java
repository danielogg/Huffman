/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 */
package applet;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.*;
import javax.swing.*;

//Classe principal que faz a simula��o do codificador Shannon Fano alem inclui os modulos para interagir com 
// a aplica��o como um jeito de praticar e aprender esta tecnica de codifica��o 
public class SimulacaoSF extends Thread
    implements Presenter, PaintCanvasClient
{
    public SimulacaoSF(AppletShannonFano target, Simbolo alphabet, int mode)
    {
    	// Variaveis locais
        keepalive = true;
        this.target = null;
        steps = null;
        controls = null;
        textarea = null;
        codetableModel = null;
        codetable = null;
        this.mode = 0;
        treecanvas = null;
        alphacanvas = null;
        command = 0;
        done = false;
        selected = null;
        alphacanvasMouseHandlerEnabled = false;
        treecanvasMouseHandlerEnabled = true;
        trainLeftChars = 0;
        trainNextTree = null;
        this.target = target;
        this.mode = mode;
        controls = target.getControlBox();
        textarea = target.getHelpArea();
        codetable = target.getCodetable();
        codetableModel = new TabelaSimbolos(alphabet.getCharacterArray());
        codetable.setModel(codetableModel);
        steps = new Arvore();
        cadena=target.getCadena();
        if(mode != 3)
        {
            alphacanvas = target.getCodepanel().getAlphabetCanvas();
            alphacanvas.setClient(this);
        }
        treecanvas = target.getCodepanel().getTreeCanvas();
        treecanvas.setClient(this);
        if(Utilities.METRICS_CONTENT == null)
        {
            Graphics2D g = (Graphics2D)treecanvas.getGraphics();
            g.setFont(Utilities.FONT_CONTENT);
            Utilities.METRICS_CONTENT = g.getFontMetrics();
            g.setFont(Utilities.FONT_FREQUENCY);
            Utilities.METRICS_FREQUENCY = g.getFontMetrics();
        }
        if(mode == 2)// modo pratico
        {
            treecanvas.addMouseListener(new MouseAdapter() {

                public void mousePressed(MouseEvent e)
                {
                    mousePressedOnTreeCanvas(e);
                }

                final SimulacaoSF this$0;
            
            {
                this$0 = SimulacaoSF.this;
            }
            }
);
            alphacanvas.addMouseListener(new MouseAdapter() {

                public void mousePressed(MouseEvent e)
                {
                    mousePressedOnAlphaCanvas(e);
                }

                final SimulacaoSF this$0;
            {
                this$0 = SimulacaoSF.this;
            }
            }
);
            alphacanvas.addMouseMotionListener(new MouseMotionAdapter() {

                public void mouseMoved(MouseEvent e)
                {
                    mouseMovedOnAlphaCanvas(e);
                }

                final SimulacaoSF this$0;
            {
                this$0 = SimulacaoSF.this;
            }
            }
);
        }
        TNodo tree = new TNodo(alphabet);
        String explanation = "No in�cio da codifica��o de Shannon-Fano, � gerada uma lista de caracteres o s�mbolos com suas frequ�ncias em ordem decrescente. Se cria a raiz da arvore com a soma total das frequ�ncias, logo ser�o divididas em duas sub arvores cuja soma seja equilibrada";
        if(mode == 1)
            explanation = (new StringBuilder(String.valueOf(explanation))).append("<br><br><b>Clique no bot�o  \"Come�ar\" ou \"Pr�ximo\" para come�ar a simula��o.</b>").toString();
        else
           explanation = (new StringBuilder(String.valueOf(explanation))).append("<br><br><b>Por favor selecione uma folha que tenha mais de um simbolo a dividir.</b>").toString();
        steps.add(tree, explanation, false);
    }
    //Ao clicar em um nodo da arvore mostrara os mensagens de aiuda
    private void mousePressedOnTreeCanvas(MouseEvent event)
    {
        if(!isTreeBuilt() && treecanvasMouseHandlerEnabled)
        {
            steps.peek().clearFlags(false);
            selected = steps.peek().contains(event.getX(), event.getY());
            if(selected != null && selected.isLeaf() && !selected.isSfLeaf())
            {
                alphacanvasMouseHandlerEnabled = true;
                steps.setLastExplanation("<b>Por favor, divida a n� em duas partes cuja soma das frequ�ncias parcias seja a mais pr�xima possivel.</b>");
                selected.setSelected(true);
            } else
            {
                steps.peek().clearFlags(false);
                steps.peek().getContent().setInteractiveBoxesLeftChars(0);
                steps.setLastExplanation("<b>Por favor selecione um n�, onde tenha mais de um s�mbolo.</b>");
                alphacanvasMouseHandlerEnabled = false;
            }
            steps.updateLastSelected();
            updateTarget();
        }
    }

    private boolean isTreeBuilt()
    {
        TNodo tree = steps.peek();
        return tree.getSfLeafs() == null || tree.getSfLeafs().length == tree.getContent().getCharacterCount();
    }
    private void mousePressedOnAlphaCanvas(MouseEvent event)
    {
        if(selected != null && alphacanvasMouseHandlerEnabled)
        {
            if(trainLeftChars > 0 && trainLeftChars < selected.getContent().getCharacterCount())
            {
                Simbolo alpha = selected.getContent();
                double mid = (double)alpha.getFrequency() / 2D;
                int freq = alpha.getFrequencySum(trainLeftChars);
                int freqprev = alpha.getFrequencySum(trainLeftChars - 1);
                int freqnext = alpha.getFrequencySum(trainLeftChars + 1);
                double dist = Math.abs(mid - (double)freq);
                double distprev = Math.abs(mid - (double)freqprev);
                double distnext = Math.abs(mid - (double)freqnext);
                if(mid - distprev <= mid - dist && mid + dist <= mid + distnext)
                    trainCreateTree();
                else
                    JOptionPane.showMessageDialog(null, "<html>Voc� escolheu um n� errado, ou n�o fez uma divis�o correta</html>", "Error", 0);
            }
        }
    }
    private void mouseMovedOnAlphaCanvas(MouseEvent event)
    {
        if(selected != null)
        {
            trainLeftChars = Math.round(((float)event.getX() - 7.5F) / 60F);
            selected.getContent().setInteractiveBoxesLeftChars(trainLeftChars);
            alphacanvas.repaint();
        }
    }

    public void paint(TypedGraphics2D tg)
    {
        steps.draw(tg, mode);
    }
    //Correr a simula��o
    public void run()
    {
        while(keepalive) 
        {
            switch(command)
            {
            case 3: // '\003'
            default:
                break;

            case 4: // '\004'
                controls.getPlayButton().setText("Parar");
                controls.getNextButton().setEnabled(false);
                controls.getPrevButton().setEnabled(false);
                if(done)
                    cleanup();
                playFullAnimation();
                controls.getPlayButton().setText("Come�ar");
                controls.getNextButton().setEnabled(!done);
                controls.getPrevButton().setEnabled(steps.getTreeCount() > 1);
                break;

            case 1: // '\001'
                done = nextShannonFanoStep();
                updateTarget();
                if(!keepalive)
                    return;
                break;

            case 2: // '\002'
                prevShannonFanoStep();
                updateTarget();
                if(!keepalive)
                    return;
                break;

            case 6: // '\006'
                treecanvasMouseHandlerEnabled = false;
                alphacanvasMouseHandlerEnabled = false;
                trainNextTree = steps.peek().copy();
                TNodo father = trainNextTree.getFirstSelectedNode();
                steps.peek().clearFlags(false);
                Simbolo alphas[] = father.getContent().splitAt(trainLeftChars);
                TNodo left = new TNodo(alphas[0]);
                TNodo right = new TNodo(alphas[1]);
                father.setChildren(left, right);
                try
                {
                    nextAnimatedShannonFanoStep(trainNextTree);
                    updateTarget();
                    if(!isTreeBuilt())
                        treecanvasMouseHandlerEnabled = true;
                    if(isTreeBuilt())
                        playFullAnimation();
                    break;
                }
                catch(InterruptedException e) { }
                if(!keepalive)
                    return;
                break;

            case 5: // '\005'
                for(; !done; done = nextShannonFanoStep());
                updateTarget();
                break;
            }
            controls.getNextButton().setEnabled(!done);
            controls.getPrevButton().setEnabled(steps.getTreeCount() > 1);
            command = 3;
            if(keepalive)
                try
                {
                    sleep(0x7fffffffL);
                }
                catch(InterruptedException interruptedexception) { }
        }
    }
    
    private void playFullAnimation()
    {
        while(!done) 
        {
            TNodo nextStepTree = steps.peek().copy();
            nextStepTree.clearFlags(false);
            doNextStep(nextStepTree);
            try
            {
                done = nextAnimatedShannonFanoStep(nextStepTree);
                updateTarget();
                sleep(1000L);
                continue;
            }
            catch(InterruptedException e)
            {
                if(!keepalive)
                    return;
                if(steps.isLastStepAnimation())
                    steps.remove();
                done = nextShannonFanoStep();
                updateTarget();
            }
            break;
        }
    }
    //Ao clicar no bot�o proximo no modo simula��o do codificador
    private boolean nextAnimatedShannonFanoStep(TNodo nextStepTree)
        throws InterruptedException
    {
        boolean retval = false;
        if(!done)
        {
            TNodo tree = steps.peek();
            if(!isTreeBuilt())
            {
                TNodo selected = nextStepTree.getFirstSelectedNode();
  
                MoverArvore mover = new MoverArvore(steps.peek(), nextStepTree, 40);
                int step = 0;
                String explanation = (new StringBuilder("O conjunto de simbolos � dividido de modo que os n�s que estejam a esquerda \"")).append(selected.getLeftChild().getContent().getString()).append("\", tenha codigo 0 e 1 para aqueles que est�o na direita  \"").append(selected.getRightChild().getContent().getString()).append("\" para ser armazenados").toString();
                while(step < 40) 
                {
                    step++;
                    animationPhaseOneStep(mover, step);
                    if(mode == 1)
                        explanation = (new StringBuilder("O n� selecionado tem frequ�ncia h=")).append(selected.getFrequency()).append(" s�o").append(" se ").append(selected.getContent().getCharacterCount()).append(" simbolo.").toString();
                    steps.setLastExplanation(explanation);
                    updateTarget();
                    sleep(50L);
                }
                for(step = 0; step < 40;)
                {
                    step++;
                    animationPhaseTwoStep(selected, step);
                    explanation = (new StringBuilder("O conjunto de simbolos � dividido de modo que os n�s que estejam a esquerda \"")).append(selected.getLeftChild().getContent().getString()).append("\", tenha codigo 0 e 1 para aqueles que est�o na direita \"").append(selected.getRightChild().getContent().getString()).append("\" para ser armazenados.").toString();
                    steps.setLastExplanation(explanation);
                    updateTarget();
                    sleep(50L);
                }

                if(step == 40 && mode == 2 && nextStepTree.getSfLeafs() != null && nextStepTree.getSfLeafs().length != nextStepTree.getContent().getCharacterCount())
                {
                    explanation = (new StringBuilder(String.valueOf(explanation))).append("<br><br><b>Por favor selecione uma folha que tenha mais de um simbolo dentro para dividir novamente</b>").toString();
                }
                if(steps.isLastStepAnimation())
                    steps.remove();
                steps.add(nextStepTree, explanation, false);
                retval = false;
            } else
            {
                tree = tree.copy();
                retval = nextHighlightStep(tree);
            }
        } else
        {
            retval = true;
        }
        return retval;
    }

    private void animationPhaseOneStep(MoverArvore mover, int step)
    {
        if(steps.isLastStepAnimation())
            steps.remove();
        steps.add(mover.animate(step), "Anima��o passo", true);
    }

    private void animationPhaseTwoStep(TNodo source, int step)
    {
        java.awt.geom.Point2D.Float m = new java.awt.geom.Point2D.Float(source.getX() + 40F, source.getY() + 40F);
        java.awt.geom.Point2D.Float l = new java.awt.geom.Point2D.Float(source.getLeftChild().getX() + 40F, source.getLeftChild().getY() + 40F);
        java.awt.geom.Point2D.Float r = new java.awt.geom.Point2D.Float(source.getRightChild().getX() + 40F, source.getRightChild().getY() + 40F);
        PintarNodo line1 = new PintarNodo(Color.BLACK, Color.BLACK, Utilities.calcLine(m.x, m.y, l.x, l.y, step <= 20 ? (double)step / 20D : 1.0D));
        PintarNodo line2 = new PintarNodo(Color.BLACK, Color.BLACK, Utilities.calcLine(m.x, m.y, r.x, r.y, step <= 20 ? (double)step / 20D : 1.0D));
        if(step > 20)
        {
            float radius = (float)(((double)(step - 20) / 20D) * 25D);
            float diameter = radius * 2.0F;
            PintarNodo circle1 = new PintarNodo(source.getLeftChild().isSfLeaf() ? Utilities.COLOR_LEAF : Utilities.COLOR_NODE, Color.BLACK, new java.awt.geom.Ellipse2D.Float(l.x - radius, l.y - radius, diameter, diameter));
            PintarNodo circle2 = new PintarNodo(source.getRightChild().isSfLeaf() ? Utilities.COLOR_LEAF : Utilities.COLOR_NODE, Color.BLACK, new java.awt.geom.Ellipse2D.Float(r.x - radius, r.y - radius, diameter, diameter));
            steps.setLastDrawables(new Drawable[] {
                line1, line2, circle1, circle2
            });
        } else
        {
            steps.setLastDrawables(new Drawable[] {
                line1, line2
            });
        }
    }

    private boolean nextShannonFanoStep()
    {
        boolean retval = false;
        if(!done)
        {
            TNodo tree = steps.peek().copy();
            tree.clearFlags(true);
            if(!isTreeBuilt())
            {
                String explanation = doNextStep(tree);
                steps.add(tree, explanation, false);
                retval = false;
            } else
            {
                retval = nextHighlightStep(tree);
            }
        } else
        {
            retval = true;
        }
        return retval;
    }
   //Depois de gerar a arvore faz a anima��o para gerar os codigos de cada simbolo 
    private boolean nextHighlightStep(TNodo tree)
    {
        boolean retval = false;
        TNodo sfLeafs[] = tree.getSfLeafs();
        int highlight = 0;
        for(int i = 0; i < sfLeafs.length; i++)
        {
            if(!sfLeafs[i].isHighlighted())
                continue;
            highlight = ++i;
            break;
        }

        tree.clearFlags(false);
        String explanation = null;
        if(highlight < sfLeafs.length)
        {
            sfLeafs[highlight].highlightPathTo();
            sfLeafs[highlight].encodePathTo();
            codetableModel.setCode(sfLeafs[highlight].getContent().getString(), sfLeafs[highlight].getCode());
            codetable.repaint();
            retval = false;
            explanation = "Depois de criar a �rvore o seguinte passo � codificar os simbolos. Para isto � preciso percorrer a �rvore desde a raiz at� a folha que representa o s�mbolo a codificar. O c�digo correspondente ser� a sequ�ncia das arestas percorrida.";
        } else
        {
            retval = true;
            sha=new Shannon();
    	    sha.codificarShannon(cadena);
    	    String codificado=sha.mostrarCodificacion(cadena);
    	    String taxa=sha.bitsNecesarios()+"/"+sha.bitsComprimidos();
    	    explanation = (new StringBuilder("A codifica��o foi completada com sucesso. Para escrever O c�digo final s� � preciso substituir cada s�mbolo da cadeina original pelo c�digo da tabela. <br><br> <b>RESULTADOS</b> <br> <b>Texto codificado:</b>"+codificado+" <br><b>Taxa de compress�o: </b>"+taxa).toString());
    	    
    	    int n = JOptionPane.showConfirmDialog(target,"Quer aprender a decodificar?", "Question", JOptionPane.YES_NO_OPTION);
    	    if(n==0)
    	    	target.boxDecodificar(codificado);
        }
        steps.add(tree, explanation, false);
        return retval;
    }
 
    private void cleanup()
    {
        codetableModel.removeAllCodes();
        codetable.repaint();
        steps.crop();
        done = false;
    }

    private String doNextStep(TNodo node)
    {
        if(!node.isLeaf())
        {
            String explanation = doNextStep(node.getLeftChild());
            if(explanation == null)
                explanation = doNextStep(node.getRightChild());
            return explanation;
        }
        Simbolo alphas[] = node.getContent().split();
        if(alphas != null)
        {
            TNodo left = new TNodo(alphas[0]);
            TNodo right = new TNodo(alphas[1]);
            node.setChildren(left, right);
            node.clearFlags(true);
            node.setSelected(true);
            return (new StringBuilder("A partir do n� selecionado com a soma das frequ�ncias h=")).append(node.getFrequency()).append(" dois n�s foram formados. O primeiro n� cont�m").append(" o s�mbolo \"").append(left.getContent().getString()).append("\" com a soma das frequ�ncias parciais h=").append(left.getContent().getFrequency()).append(", o ").append("segundo n� inclui \"").append(right.getContent().getString()).append("\" e tem a soma das frequ�ncias h=").append(right.getContent().getFrequency()).append(".").toString();
        } else
        {
            return null;
        }
    }

    private void prevShannonFanoStep()
    {
        steps.remove();
        codetableModel.removeCode();
        codetable.repaint();
        done = false;
        updateTarget();
    }

    private void trainCreateTree()
    {
        command = 6;
        interrupt();
    }

    public void updateTarget()
    {
        textarea.setHtmlText(steps.getLastExplanation());
        treecanvas.setPreferredSize(steps.getLastDimension());
        treecanvas.revalidate();
        treecanvas.repaint();
        if(alphacanvas != null)
        {
            alphacanvas.setPreferredSize(steps.getLastAlphaDimension());
            alphacanvas.revalidate();
            alphacanvas.repaint();
        }
    }

    public int getMode()
    {
        return mode;
    }

    public void kill()
    {
        keepalive = false;
        interrupt();
    }

    public void playPause()
    {
        command = 4;
        interrupt();
    }

    public void next()
    {
        command = 1;
        interrupt();
    }

    public void prev()
    {
        command = 2;
        interrupt();
    }

    public void show()
    {
        command = 5;
        interrupt();
    }
    
    
    private boolean keepalive;
    private AppletShannonFano target;
    private Arvore steps;
    private BotoesControle controls;
    private LazyJEditorPane textarea;
    private TabelaSimbolos codetableModel;
    private JTable codetable;
    private int mode;
    private PaintCanvas treecanvas;
    private PaintCanvas alphacanvas;
    private int command;
    private boolean done;
    TNodo selected;
    private boolean alphacanvasMouseHandlerEnabled;

    private boolean treecanvasMouseHandlerEnabled;
    private int trainLeftChars;
    private TNodo trainNextTree;
    private String cadena="";
    private Shannon sha;    
}
