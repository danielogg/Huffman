/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */

package applet;


public class Caracter
    implements Cloneable
{

    public Caracter(char content, int frequency)
    {
        this.content = content;
        this.frequency = frequency;
    }

    public Caracter(Caracter source)
    {
        content = source.content;
        frequency = source.frequency;
    }

    public int getFrequency()
    {
        return frequency;
    }

    public void setFrequency(int frequency)
    {
        this.frequency = frequency;
    }

    public char getChar()
    {
        return content;
    }

    public String toString()
    {
        return (new StringBuilder("\"")).append(content).append("\" h=").append(frequency).toString();
    }

    public boolean equals(Object obj)
    {
        if(obj instanceof Caracter)
            return equals((Caracter)obj);
        else
            return false;
    }

    public boolean equals(Caracter rhs)
    {
        return content == rhs.content && frequency == rhs.frequency;
    }

    private char content;
    private int frequency;
}
