/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 *  
 */
package applet;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.applet.*;

//Classe de inicio do programa, desenha a interface principal de usuario
public class AppletShannonFano extends Applet
{
	//Atributos
	private JTextField tbTexto;
	private JRadioButton SimulacaoButton;
	private JRadioButton praticarButton;
	private ButtonGroup group;
	private String allowedChars;
	private int modo=1;  //1 simulacao, 0 pratico interativo
	private Shannon sha;
	private String codificado;
	private JSplitPane vSplitter;
	private JSplitPane hSplitter;
	private Codepanels codepanel;
	private JTable codetable;
	private BotoesControle animationControl;
	private LazyJEditorPane helpArea;
	private Presenter presenter;
    protected JButton next;
    protected JButton prev;
    protected LazyJEditorPane aCodigo;
    protected LazyJEditorPane aSalida;
    int contador=0, limite=0, cont=0,lim=0;
    String resultados="" ,codigo="" ,taxa="",decodicado="";
    String valor[];
    
    // Metodo que e executado ao iniciar o applet
	public void init()
	{
		//Desehna a interface de usu�rio
		JPanel paTop=new JPanel();
		JLabel laTitulo = new JLabel();
		JLabel laTitulo2 = new JLabel();
		paTop.setBackground(Color.BLACK);
		
		laTitulo.setFont(new java.awt.Font("Tahoma", 1, 18));
	    laTitulo.setForeground(Color.WHITE);
	    laTitulo.setText("Codificador/Decodificador Shannon Fano");
	    paTop.add(laTitulo);
	    
	    laTitulo2.setFont(new java.awt.Font("Tahoma", 1, 14));
	    laTitulo2.setText("Insira o texto:");
	
	    tbTexto=new JTextField(10);
	    tbTexto.setFont(new java.awt.Font("Tahoma", 1, 11));
	    tbTexto.setPreferredSize(new Dimension(70, 20));
	    tbTexto.setColumns(60);
	    
	    SimulacaoButton = new JRadioButton("Simula��o");
	    SimulacaoButton.setSelected(true);
	    SimulacaoButton.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent evt) {
	        	SimulacaoButtonActionPerformed(evt);
	        }
	    });
	  
	    praticarButton = new JRadioButton("Praticar");
	    praticarButton.setSelected(false);
	    praticarButton.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent evt) {
	        	praticarButtonActionPerformed(evt);
	        }
	    });
	    
	    group = new ButtonGroup();
	    group.add(SimulacaoButton);
	    group.add(praticarButton);
	    allowedChars = "abcdefghijklmnopqrstuvwxyz0123456789 ";
	
	    JPanel paBody = new JPanel();
	    JPanel paBody2 = new JPanel();
	    JButton btCodificar =new JButton("Codificar");
	
	    btCodificar.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent evt) {
	            btCodificarActionPerformed(evt);
	        }
	    });
	    paTop.setPreferredSize(new Dimension(2000, 30));
	    paBody.setPreferredSize(new Dimension(2000, 35));
	    paBody2.setPreferredSize(new Dimension(2000, 40));
	
	    paTop.add(laTitulo);
	    paBody.add(laTitulo2);
	    paBody.add(tbTexto);
	    paBody.add(btCodificar);
	    paBody2.add(SimulacaoButton);
	    paBody2.add(praticarButton);
	
	    JPanel paSuperior= new JPanel();
	    paSuperior.setPreferredSize(new Dimension(2000, 115));
	
	    paSuperior.add(paTop,"North");
	    paSuperior.add(paBody,"Center");
	    paSuperior.add(paBody2,"South");
	    add("north", paSuperior);
		
	    vSplitter = null;
	    hSplitter = null;
	    codepanel = null;
	    codetable = null;
	    animationControl = null;
	    helpArea = null;
	    presenter = null;
	    setSize(1000, 700);
	    codepanel = new Codepanels(2);
	    codepanel.setMinimumSize(new Dimension(500, 550));//novo
	    codetable = new JTable();
	    JPanel codetablePanel = new JPanel();
	    codetablePanel.setLayout(new BorderLayout());
	    codepanel.revalidate();
	
	    codetable.setMinimumSize(new Dimension(200, 250));//novo
	    
	    JScrollPane codetableScroll = new JScrollPane(codetable);
	    codetablePanel.setPreferredSize(new Dimension(250, 250));
	    codetablePanel.add(codetableScroll, "Center");
	    codetablePanel.setBorder(BorderFactory.createTitledBorder("Tabela de s�mbolos"));
	    
 	    codetablePanel.setMinimumSize(new Dimension(200, 260));//novo
	    
	    hSplitter = new JSplitPane(1, codetablePanel, createControlHelpBox());
	    hSplitter.setResizeWeight(0.5D);
	    vSplitter = new JSplitPane(1, codepanel, hSplitter);
	    vSplitter.setResizeWeight(1.0D);
	    this.add(vSplitter);
	}
	
	//Ao clicar no bot�o proximo na decodifica��o mostra os resultados parciais 
    public void eventoNext(ActionEvent evt)
    {
        if(cont>=-1 && cont<lim)
        {
        	cont++;
        	prev.setEnabled(true);
            resultados+=sha.getCaracter(valor[cont]);
            aCodigo.setText("<b>Cadeia codificada:</b> "+codificado+"  <br> O c�digo: "+valor[cont]+" de acordo � tabela tem simbolo="+sha.getCaracter(valor[cont]));
            aSalida.setText(resultados);
            
            if(cont==lim-1)
            {
            	next.setEnabled(false);
                JOptionPane.showMessageDialog(this,"Decodifica��o finalizada");
                return;
            }
        }
    }
	//Ao clicar no bot�o anterior na decodifica��o mostra os resultados parciais
    public void eventoPrev(ActionEvent evt)
    {
    	if(cont>=0 && cont<lim)
        {
            
            resultados=resultados.substring(0, resultados.length()-1);
            cont--;
            if (cont<0) 
            	cont=0;
            aCodigo.setText("<b>Cadeia codificada:</b> "+codificado+"  <br> O c�digo: "+valor[cont]+" de acordo � tabela tem simbolo="+sha.getCaracter(valor[cont]));
            aSalida.setText(resultados);
            prev.setEnabled(true);
            next.setEnabled(true);
            if(cont==0)
            {
            	prev.setEnabled(false);
            	aCodigo.setText("<b>Cadeia codificada:</b> "+codificado );
            	resultados="";
            	aSalida.setText(resultados);
            	cont=-1;
            }
        }
    }
	// Metodo que desenha a interface com os componentes necessarios para a decodifica��o
	public void boxDecodificar(String codigo)
	{
	    next = new JButton("Pr�ximo");
	    next.setSelected(true);
	    next.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent evt) {
	        	eventoNext(evt);
	        }
	    });
	    prev = new JButton("Anterior");
	    prev.setSelected(true);
	    prev.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent evt) {
	        	eventoPrev(evt);
	        }
	    });
	    prev.setEnabled(false);
	    
	    sha.decodificar(codigo);
        decodicado=sha.getValor();
        valor= decodicado.split(":");
        lim=valor.length;
        cont=-1;
        resultados="";
        
	    JLabel laTitulo=new JLabel("Decodifica��o:");
	    laTitulo.setFont(new java.awt.Font("Tahoma", 1, 14));
	    
	    aCodigo= new JEditorPane("text/html", ""); //era LazyJEditorPane
	    aCodigo.setEditable(false);
	    aCodigo.setText("<b>Cadeia codificada:</b> "+codigo );
	    aCodigo.setMaximumSize(new Dimension(200, 550));
	    JScrollPane aCodigoScroll = new JScrollPane(aCodigo);
	    JPanel aCodigoPanel = new JPanel(new GridLayout());
	    aCodigoScroll.setMaximumSize(new Dimension(200, 550));
	    aCodigoPanel.add(aCodigoScroll);
	    aCodigoPanel.setAlignmentY(0.5F);
	    aCodigoPanel.setBorder(BorderFactory.createTitledBorder("C�digo"));
	    
	    Box box = new Box(1);
	    box.setPreferredSize(new Dimension(300, 200));
	    
	    aSalida = new JEditorPane("text/html", ""); //era LazyJEditorPane
	    aSalida.setEditable(false);
	    JScrollPane helpAreaScroll = new JScrollPane(aSalida);
	    JPanel helpAreaPanel = new JPanel(new GridLayout());
	    helpAreaScroll.setMaximumSize(new Dimension(200, 550));
	    helpAreaPanel.add(helpAreaScroll);
	    helpAreaPanel.setAlignmentY(0.5F);
	    helpAreaPanel.setBorder(BorderFactory.createTitledBorder("Saida"));
	    helpAreaPanel.setPreferredSize(new Dimension(250, 250));
	    box.add(laTitulo);
	    box.add(aCodigoPanel);
	    box.add(next);
	    box.add(prev);
	    box.add(helpAreaPanel);
	    hSplitter.setRightComponent(box);
	    vSplitter.setRightComponent(hSplitter);
	}
	// Metodo que desenha os componentes donde sera mostrado os resultados parciais da codifica��o
	private Box createControlHelpBox()
	{
	    Box box = new Box(1);
	    box.setPreferredSize(new Dimension(720, 550));
	    box.setMinimumSize(new Dimension(250, 250));//novo
	    animationControl = new BotoesControle();
	    helpArea = new LazyJEditorPane("text/html", "");
	    helpArea.setEditable(false);
	    JScrollPane helpAreaScroll = new JScrollPane(helpArea);
	    JPanel helpAreaPanel = new JPanel(new GridLayout());
	    helpAreaPanel.add(helpAreaScroll);
	    helpAreaPanel.setAlignmentY(0.5F);
	    helpAreaPanel.setBorder(BorderFactory.createTitledBorder("Explica��o"));
	    helpAreaPanel.setPreferredSize(new Dimension(250, 250));
	    box.add(animationControl);
	    box.add(Box.createRigidArea(new Dimension(50, 10)));
	    box.add(helpAreaPanel);
	    return box;
	}
	
	
	public void setPresenter(Presenter presenter)
	{
	    if(this.presenter != null)
	    {
	        this.presenter.kill();
	        while(this.presenter.isAlive()) 
	        {
	            try
	            {
	                Thread.sleep(100L);
	                continue;
	            }
	            catch(InterruptedException e) { }
	            break;
	        }
	    }
	    this.presenter = presenter;
	    animationControl.setPresenter(this.presenter);
	    this.presenter.updateTarget();
	}
	
	
	//Evento ao clicar o bot�o codificar
	public void btCodificarActionPerformed(ActionEvent actionevent)
	{
		 String s=tbTexto.getText();
		 if(s != null && s.compareTo("") != 0)
	     {
	         s = s.toLowerCase();
	         if(!checkInput(s))
	         {
	        	 JOptionPane.showMessageDialog(this, "S� � permitido cadeia de caracteres alfanumericos [a-z] e [0-9]", "Error", 0);
	         }
	         else
	         {
	     	    hSplitter.setRightComponent(createControlHelpBox());
	     	    vSplitter.setRightComponent(hSplitter);
        	    Presenter presenter = null;
        		Simbolo alpha=new Simbolo(s);
        		Codepanels cp = null;
        		cp = new Codepanels(2);
        		setCodepanels(cp);
        		presenter = new SimulacaoSF(this, alpha, modo);
        		setPresenter(presenter);
        	    presenter.start();
        	    
        	    sha=new Shannon();
        	    sha.codificarShannon(s);
        	    codificado=sha.mostrarCodificacion(s);
	         }
	     }
		 else
	    	 JOptionPane.showMessageDialog(this, "Insere uma cadeia para codificar", "Error", 0);
	}
	
	// Metodo que faz a valida�ao da cadeia de entrada, so e permitido valores alfanumericos
	private boolean checkInput(String s)
	{
	    for(int i = 0; i < s.length(); i++)
	    {
	        char c = s.charAt(i);
	        boolean flag = false;
	        for(int j = 0; j < allowedChars.length(); j++)
	            if(c == allowedChars.charAt(j))
	                flag = true;
	        if(!flag)
	            return false;
	    }
	    return true;
	}
	// Modificadores e selectores dos componentes mais importantes
	public JTable getCodetable()
	{
	    return codetable;
	}
	
	public Codepanels getCodepanel()
	{
	    return codepanel;
	}
	
	public LazyJEditorPane getHelpArea()
	{
	    return helpArea;
	}
	
	public Presenter getPresenter()
	{
	    return presenter;
	}
	
	public BotoesControle getControlBox()
	{
	    return animationControl;
	}
	public String getCadena()
	{
	    return tbTexto.getText();
	}
	public void setCodepanels(Codepanels panel)
	{
	    codepanel = panel;
	    vSplitter.setLeftComponent(codepanel);
	    vSplitter.setDividerLocation(vSplitter.getDividerLocation());
	    codepanel.revalidate();
	}
	
	//variavel que armazena os simbolos permitidos
	public void setAllowedChar(String s)
	{
	    allowedChars = s;
	}

	public void SimulacaoButtonActionPerformed(ActionEvent actionevent)
	{
		modo=1; //Modo Simula��o
	}
	public void praticarButtonActionPerformed(ActionEvent actionevent)
	{
		modo=2; // Modo Pratica 
	}
}