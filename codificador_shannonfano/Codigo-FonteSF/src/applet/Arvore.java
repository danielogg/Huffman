/**
 *
 * Codificador - Decodificador Shannon Fano
 * Desenvolvido para a disciplina de Fundamentos de Sistemas Multimidia
 * Alunos:
 * -Giomar Oliver Sequeiros Olivera
 * -Edhelmira Lima Medina
 * Semestre 2011-1
 */
package applet;

import java.awt.Dimension;
import java.awt.Graphics2D;

//Classe arvore encarregada de armazenar e desenhar a arvore de shannon fano 
class Arvore
{
    private class TreeListNode
    {
        public void draw(Graphics2D g)
        {
            if(drawables != null)
            {
                for(int i = 0; i < drawables.length; i++)
                    drawables[i].drawAndFill(g);

            }
            content.draw(g);
        }
        private void showAlphabet(Graphics2D g, boolean interactive)
        {
            if(selected != null)
                selected.getContent().draw(g, interactive);
        }

        private TNodo content;
        private TreeListNode prev;
        private String explanation;
        private boolean animationStep;
        private Drawable drawables[];
        private TNodo selected;
        final Arvore this$0;

        public TreeListNode(TNodo content, String explanation, boolean animationStep, Drawable drawables[], TreeListNode prev)
        {
            this$0 = Arvore.this;
            this.content = null;
            this.prev = null;
            this.explanation = null;
            this.animationStep = false;
            this.drawables = null;
            selected = null;
            this.content = content;
            this.explanation = explanation;
            this.animationStep = animationStep;
            this.drawables = drawables;
            this.prev = prev;
            selected = this.content.getFirstSelectedNode();
        }
    }


    Arvore()
    {
        trees = 0;
        last = null;
    }

    public void add(TNodo tree, String explanation, boolean animationStep)
    {
        last = new TreeListNode(tree, explanation, animationStep, null, last);
        trees++;
    }

    public void add(TNodo tree, String explanation, Drawable drawables[], boolean animationStep)
    {
        last = new TreeListNode(tree, explanation, animationStep, drawables, last);
        trees++;
    }

    public String getLastExplanation()
    {
        return last != null ? last.explanation : null;
    }

    public void setLastExplanation(String explanation)
    {
        if(last != null)
            last.explanation = explanation;
    }

    public void remove()
    {
        if(last != null)
        {
            last = last.prev;
            trees--;
        }
    }

    public void crop()
    {
        for(int i = trees; i > 1; i--)
            remove();

    }

    public TNodo peek()
    {
        return last != null ? last.content : null;
    }

    public int getTreeCount()
    {
        return trees;
    }

    public boolean isLastStepAnimation()
    {
        return last != null ? last.animationStep : false;
    }

    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        for(TreeListNode cursor = last; cursor != null; cursor = cursor.prev)
        {
            sb.append(cursor.animationStep);
            sb.append("  ");
            sb.append(cursor.content);
            sb.append(" / ");
        }

        return sb.toString();
    }

    public void draw(TypedGraphics2D tg, int mode)
    {
        if(last != null)
            if(tg.getType() == 3)
                last.draw(tg.getGraphics());
            else
                last.showAlphabet(tg.getGraphics(), mode == 2);
    }

    public void setLastDrawables(Drawable drawables[])
    {
        if(last != null)
            last.drawables = drawables;
    }

    public Dimension getLastDimension()
    {
        return last != null ? last.content.getDimension() : null;
    }

    public Dimension getLastAlphaDimension()
    {
        return last != null && last.selected != null ? last.selected.getAlphaDimension() : null;
    }

    public void updateLastSelected()
    {
        if(last != null)
            last.selected = last.content.getFirstSelectedNode();
    }

    private int trees;
    private TreeListNode last;
}
