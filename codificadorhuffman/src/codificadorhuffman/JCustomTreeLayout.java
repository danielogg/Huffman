package codificadorhuffman;

import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.Forest;
import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.Map;

public class JCustomTreeLayout
  extends TreeLayout
{
  public JCustomTreeLayout(Forest forest)
  {
    super(forest);
  }
  
  public JCustomTreeLayout(Forest forest, int distX, int distY)
  {
    super(forest, distX, distY);
  }
  
  public void setSize(Dimension size) {}
  
  protected void AjustarPosicoesDosVerticesDaArvore(No noRaiz)
  {
    Point2D ptCenter = new Point2D.Double();
    Point2D ptRoot = (Point2D)this.locations.get(noRaiz);
    ptCenter.setLocation(getSize().getWidth() / 2.0D, ptRoot.getY());
    
    setLocation(noRaiz, ptCenter);
    
    AjustarPosicoesDoVertice(noRaiz);
    
    CalcularReposicionamentoRecursivo(noRaiz);
  }
  
  protected void AjustarPosicoesDoVertice(No noRaiz)
  {
    No noEsquerdo = noRaiz.getNoEsquerdo();
    
    No noDireito = noRaiz.getNoDireito();
    if (noEsquerdo != null)
    {
      Point2D ptEsquerdo = (Point2D)this.locations.get(noEsquerdo);
      Point2D ptRaiz = (Point2D)this.locations.get(noRaiz);
      
      ptEsquerdo.setLocation(ptRaiz.getX() - DEFAULT_DISTX / 2, ptEsquerdo.getY());
      
      setLocation(noEsquerdo, ptEsquerdo);
      
      AjustarPosicoesDoVertice(noEsquerdo);
    }
    if (noDireito != null)
    {
      Point2D ptRight = (Point2D)this.locations.get(noDireito);
      Point2D ptRoot = (Point2D)this.locations.get(noRaiz);
      
      ptRight.setLocation(ptRoot.getX() + DEFAULT_DISTX / 2, ptRight.getY());
      
      setLocation(noDireito, ptRight);
      
      AjustarPosicoesDoVertice(noDireito);
    }
  }
  
  protected double getMinimumXLeft(No nodeRoot, No nodeParent)
  {
    if (nodeRoot == null) {
      return 2.147483647E9D;
    }
    if ((nodeRoot.getNoEsquerdo() == null) && (nodeRoot.getNoDireito() == null))
    {
      if (nodeParent.getNoEsquerdo() == nodeRoot) {
        return ((Point2D)this.locations.get(nodeRoot)).getX();
      }
      return 2.147483647E9D;
    }
    double dMinLeft = getMinimumXLeft(nodeRoot.getNoEsquerdo(), nodeRoot);
    double dMinRight = getMinimumXLeft(nodeRoot.getNoDireito(), nodeRoot);
    
    return dMinLeft < dMinRight ? dMinLeft : dMinRight;
  }
  
  protected void MoveAllVertices(double dInc, No nodeRoot)
  {
    if (nodeRoot != null)
    {
      Point2D ptRoot = (Point2D)this.locations.get(nodeRoot);
      
      ptRoot.setLocation(ptRoot.getX() + dInc, ptRoot.getY());
      
      setLocation(nodeRoot, ptRoot);
      
      MoveAllVertices(dInc, nodeRoot.getNoEsquerdo());
      
      MoveAllVertices(dInc, nodeRoot.getNoDireito());
    }
  }
  
  private void CalcularReposicao(No noRaiz, Point2D ptCentro)
  {
    double dMaxRight = getMaximunXRight(noRaiz.getNoEsquerdo(), noRaiz);
    if ((dMaxRight != 0.0D) && (dMaxRight >= ptCentro.getX()))
    {
      double dDec = ptCentro.getX() - dMaxRight;
      dDec -= DEFAULT_DISTX;
      MoveAllVertices(dDec, noRaiz.getNoEsquerdo());
    }
    double dMinLeft = getMinimumXLeft(noRaiz.getNoDireito(), noRaiz);
    if ((dMinLeft != 2.147483647E9D) && (dMinLeft <= ptCentro.getX()))
    {
      double dInc = ptCentro.getX() - dMinLeft;
      dInc += DEFAULT_DISTX;
      MoveAllVertices(dInc, noRaiz.getNoDireito());
    }
  }
  
  private double getMaximunXRight(No noRaiz, No noPai)
  {
    if (noRaiz == null) {
      return 0.0D;
    }
    if ((noRaiz.getNoEsquerdo() == null) && (noRaiz.getNoDireito() == null))
    {
      if (noPai.getNoDireito() == noRaiz) {
        return ((Point2D)this.locations.get(noRaiz)).getX();
      }
      return 0.0D;
    }
    double dMaxLeft = getMaximunXRight(noRaiz.getNoEsquerdo(), noRaiz);
    double dMaxRight = getMaximunXRight(noRaiz.getNoDireito(), noRaiz);
    
    return dMaxLeft < dMaxRight ? dMaxRight : dMaxLeft;
  }
  
  protected void CalcularReposicionamentoRecursivo(No noRaiz)
  {
    if ((noRaiz != null) && (
      (noRaiz.getNoEsquerdo() != null) || (noRaiz.getNoDireito() != null)))
    {
      CalcularReposicionamentoRecursivo(noRaiz.getNoEsquerdo());
      
      CalcularReposicionamentoRecursivo(noRaiz.getNoDireito());
      
      CalcularReposicao(noRaiz, (Point2D)this.locations.get(noRaiz));
    }
  }
}
