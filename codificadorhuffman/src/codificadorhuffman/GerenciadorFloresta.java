package codificadorhuffman;

import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.Forest;
import edu.uci.ics.jung.graph.Tree;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GerenciadorFloresta
{
  private DelegateForest<No, Aresta> floresta;
  private List<List<DelegateTree<No, Aresta>>> listaDeArvoresPorPassos;
  private List<List<Frequencia>> dadosFrequenciaPorPassos;
  
  public GerenciadorFloresta(List<List<String>> dadosFrequenciaPassoInicial)
  {
    this.floresta = new DelegateForest();
    this.listaDeArvoresPorPassos = new ArrayList();
    
    this.dadosFrequenciaPorPassos = new ArrayList();
    
    List<Frequencia> vetorFrequenciaNoPasso = new ArrayList();
    for (List<String> d : dadosFrequenciaPassoInicial)
    {
      Frequencia f = new Frequencia((String)d.get(0), (String)d.get(1));
      vetorFrequenciaNoPasso.add(f);
    }
    this.dadosFrequenciaPorPassos.add(vetorFrequenciaNoPasso);
  }
  
  public DelegateForest<No, Aresta> getFloresta()
  {
    return this.floresta;
  }
  
  public void setFloresta(DelegateForest<No, Aresta> floresta)
  {
    this.floresta = floresta;
  }
  
  public void ordenarVetor(List<Frequencia> vetor, int tipo)
  {
    for (int i = 0; i < vetor.size() - 1; i++)
    {
      int indMaximo = i;
      for (int j = i + 1; j < vetor.size(); j++) {
        if (tipo == 1)
        {
          if (Integer.valueOf(((Frequencia)vetor.get(j)).getFrequencia()).intValue() > Integer.valueOf(((Frequencia)vetor.get(indMaximo)).getFrequencia()).intValue()) {
            indMaximo = j;
          }
        }
        else if ((tipo == 2) && 
          (((Frequencia)vetor.get(j)).getRotulo().length() > ((Frequencia)vetor.get(i)).getRotulo().length())) {
          indMaximo = j;
        }
      }
      if (i != indMaximo)
      {
        String rotulo = ((Frequencia)vetor.get(i)).getRotulo();
        String valor = ((Frequencia)vetor.get(i)).getFrequencia();
        
        ((Frequencia)vetor.get(i)).setRotulo(((Frequencia)vetor.get(indMaximo)).getRotulo());
        ((Frequencia)vetor.get(i)).setFrequencia(((Frequencia)vetor.get(indMaximo)).getFrequencia());
        
        ((Frequencia)vetor.get(indMaximo)).setRotulo(rotulo);
        ((Frequencia)vetor.get(indMaximo)).setFrequencia(valor);
      }
    }
  }
  
  public void montarVetorFrequenciaNoPasso(int passo)
  {
    if (passo > this.dadosFrequenciaPorPassos.size() - 1)
    {
      List<Frequencia> vetorFrequenciaAnterior = (List)this.dadosFrequenciaPorPassos.get(passo - 1);
      List<Frequencia> vetorFrequenciaAtual = new ArrayList();
      
      int tamanho = vetorFrequenciaAnterior.size();
      String rotulo = ((Frequencia)vetorFrequenciaAnterior.get(tamanho - 1)).getRotulo() + ((Frequencia)vetorFrequenciaAnterior.get(tamanho - 2)).getRotulo();
      
      Integer valor = Integer.valueOf(Integer.valueOf(((Frequencia)vetorFrequenciaAnterior.get(tamanho - 1)).getFrequencia()).intValue() + Integer.valueOf(((Frequencia)vetorFrequenciaAnterior.get(tamanho - 2)).getFrequencia()).intValue());
      String frequencia = valor.toString();
      
      Frequencia f = new Frequencia(rotulo, frequencia);
      vetorFrequenciaAtual.add(f);
      for (int i = 0; i < vetorFrequenciaAnterior.size() - 2; i++)
      {
        f = new Frequencia(((Frequencia)vetorFrequenciaAnterior.get(i)).getRotulo(), ((Frequencia)vetorFrequenciaAnterior.get(i)).getFrequencia());
        vetorFrequenciaAtual.add(f);
      }
      ordenarVetor(vetorFrequenciaAtual, 1);
      this.dadosFrequenciaPorPassos.add(vetorFrequenciaAtual);
    }
  }
  
  private DelegateTree<No, Aresta> montarArvoreNoPasso(int passo, int pos)
  {
    DelegateTree<No, Aresta> t = null;
    if (this.dadosFrequenciaPorPassos != null)
    {
      if (passo == 0)
      {
        t = new DelegateTree();
        
        No no = new No(((Frequencia)((List)this.dadosFrequenciaPorPassos.get(passo)).get(pos)).getRotulo(), null, null);
        t.addVertex(no);
      }
      else
      {
        Frequencia fAtual = (Frequencia)((List)this.dadosFrequenciaPorPassos.get(passo)).get(pos);
        
        t = new DelegateTree();
        No no = new No(fAtual.getRotulo(), null, null);
        t.addVertex(no);
        if (no.getRotulo().length() > 1) {
          montarRecursivamenteArvore(passo, no, t);
        }
      }
      return t;
    }
    return null;
  }
  
  private void montarRecursivamenteArvore(int passo, No noAtual, DelegateTree<No, Aresta> t)
  {
    passo--;
    List<Frequencia> vetorFrequenciasAnterior = (List)this.dadosFrequenciaPorPassos.get(passo);
    
    int aresta = 0;
    int primeiro = 1;
    No noAnterior = null;
    for (Frequencia f : vetorFrequenciasAnterior) {
      if (noAtual.getRotulo().contains(f.getRotulo()))
      {
        if (!noAtual.getRotulo().equalsIgnoreCase(f.getRotulo()))
        {
          noAnterior = new No(f.getRotulo(), null, null);
          t.addChild(new Aresta(aresta), noAtual, noAnterior);
          if (aresta == 0)
          {
            noAtual.setNoEsquerdo(noAnterior);
            aresta = 1;
          }
          else
          {
            noAtual.setNoDireito(noAnterior);
          }
        }
        else
        {
          noAnterior = noAtual;
        }
        if (f.getRotulo().length() > 1) {
          montarRecursivamenteArvore(passo, noAnterior, t);
        }
      }
    }
  }
  
  public void montarFlorestaNoPasso(int passo)
  {
    List<DelegateTree<No, Aresta>> vetorDeArvores = null;
    if (passo > this.listaDeArvoresPorPassos.size() - 1) {
      for (int i = 0; i < ((List)this.dadosFrequenciaPorPassos.get(passo)).size(); i++)
      {
        DelegateTree<No, Aresta> t = montarArvoreNoPasso(passo, i);
        if (passo > this.listaDeArvoresPorPassos.size() - 1)
        {
          vetorDeArvores = new ArrayList();
          this.listaDeArvoresPorPassos.add(vetorDeArvores);
        }
        ((List)this.listaDeArvoresPorPassos.get(passo)).add(t);
      }
    }
    limparFloresta();
    for (DelegateTree<No, Aresta> t : this.listaDeArvoresPorPassos.get(passo)) {
      this.floresta.addTree(t);
    }
  }
  
  private void limparFloresta()
  {
    Collection<Tree<No, Aresta>> lista = this.floresta.getTrees();
    for (Tree<No, Aresta> t : lista) {
      this.floresta.removeVertex(t.getRoot());
    }
  }
  
  public List<List<Frequencia>> getDadosFrequenciaPorPassos()
  {
    return this.dadosFrequenciaPorPassos;
  }
  
  public void setDadosFrequenciaPorPassos(List<List<Frequencia>> dadosFrequenciaPorPassos)
  {
    this.dadosFrequenciaPorPassos = dadosFrequenciaPorPassos;
  }
  
  public List<List<DelegateTree<No, Aresta>>> getListaDeArvoresPorPassos()
  {
    return this.listaDeArvoresPorPassos;
  }
  
  public void setListaDeArvoresPorPassos(List<List<DelegateTree<No, Aresta>>> listaDeArvoresPorPassos)
  {
    this.listaDeArvoresPorPassos = listaDeArvoresPorPassos;
  }
}
