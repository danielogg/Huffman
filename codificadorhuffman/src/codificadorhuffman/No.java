package codificadorhuffman;

public class No
{
  private String rotulo;
  private No noEsquerdo;
  private No noDireito;
  
  public No(String rotulo, No noEsquerdo, No noDireito)
  {
    this.rotulo = rotulo;
    this.noEsquerdo = noEsquerdo;
    this.noDireito = noDireito;
  }
  
  public No() {}
  
  public No getNoDireito()
  {
    return this.noDireito;
  }
  
  public void setNoDireito(No noDireito)
  {
    this.noDireito = noDireito;
  }
  
  public No getNoEsquerdo()
  {
    return this.noEsquerdo;
  }
  
  public void setNoEsquerdo(No noEsquerdo)
  {
    this.noEsquerdo = noEsquerdo;
  }
  
  public String getRotulo()
  {
    return this.rotulo;
  }
  
  public void setRotulo(String rotulo)
  {
    this.rotulo = rotulo;
  }
  
  public String toString()
  {
    return this.rotulo;
  }
}
