package codificadorhuffman;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Ellipse2D.Float;
import org.apache.commons.collections15.Transformer;

public class JCustomVertexShapeTransformer
  implements Transformer
{
  public Ellipse2D transform(Object arg0)
  {
    return new Ellipse2D.Float(-15.0F, -15.0F, 30.0F, 30.0F);
  }
}
