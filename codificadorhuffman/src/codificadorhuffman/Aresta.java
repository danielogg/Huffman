package codificadorhuffman;

public class Aresta
{
  private int lado;
  
  public Aresta(int lado)
  {
    this.lado = lado;
  }
  
  public int getLado()
  {
    return this.lado;
  }
  
  public void setLado(int lado)
  {
    this.lado = lado;
  }
  
  public String toString()
  {
    return String.valueOf(this.lado);
  }
}
