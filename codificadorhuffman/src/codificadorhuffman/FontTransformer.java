package codificadorhuffman;

import java.awt.Font;
import org.apache.commons.collections15.Transformer;

class FontTransformer
  implements Transformer
{
  public Font transform(Object i)
  {
    return new Font("Comic Sans", 0, 14);
  }
}
