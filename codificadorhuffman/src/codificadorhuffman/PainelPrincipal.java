package codificadorhuffman;

import edu.uci.ics.jung.graph.*;
import edu.uci.ics.jung.visualization.*;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.table.OtherTableModel;

// Referenced classes of package codificadorhuffman:
//            Frequencia, JCustomTreeLayout, FontTransformer, JCustomEdgeLabelClosenessTransformer, 
//            JCustomVertexFillPaintTransformer, JCustomVertexShapeTransformer, No, GerenciadorFloresta

public class PainelPrincipal extends JApplet
{

    private ArrayList dadosLinhaTabela;
    private GerenciadorFloresta gerenciadorFloresta;
    private int passoCorrente;
    private VisualizationViewer vv;
    private GraphZoomScrollPane painelComRolagem;
    private ButtonGroup buttonGroup1;
    private ButtonGroup buttonGroup2;
    private ButtonGroup buttonGroup3;
    private JButton jBotaoAnteriorCodificacao;
    private JButton jBotaoAnteriorDecodificacao;
    private JButton jBotaoCodificar;
    private JButton jBotaoDecodificar;
    private JButton jBotaoImportarDecodificacao;
    private JButton jBotaoInicioCodificacao;
    private JButton jBotaoInicioDecodificacao;
    private JButton jBotaoProximoCodificacao;
    private JButton jBotaoProximoDecodificacao;
    private JButton jBotaoSeguinte;
    private JButton jBotaoVoltarDecodificacaoInicio;
    private JButton jButtonExportarCodificacao;
    private JButton jButtonVoltarCodificacao;
    private JDialog jDialog1;
    private JFrame jFrame1;
    private JButton jGerarCodigoCodificacao;
    private JLabel jLabel1;
    private JLabel jLabel10;
    private JLabel jLabel11;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JLabel jLabelTaxaCompactacao;
    private JLabel jLabelTextoDecodificado;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JPanel jPanel6;
    private JPanel jPanel7;
    private JRadioButton jRadioButtonCodificacao;
    private JRadioButton jRadioButtonDecodificacao;
    private JRadioButton jRadioButtonDiretoDecodificacao;
    private JRadioButton jRadioButtonExecucaoDiretaCodificacao;
    private JRadioButton jRadioButtonExecucaoPassoAPassoCodificacao;
    private JRadioButton jRadioButtonPassoAPassoDecodificacao;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JScrollPane jScrollPane3;
    private JTable jTabelaDeFrequenciaCodificacaoCorrente;
    private JTable jTabelaDeFrequenciaCodificacaoOriginal;
    private JTable jTabelaDeFrequenciaDecodificacaoOriginal;
    private JTextField jTextFieldArquivoImportado;
    private JTextField jTextoCodificadoCodificacao;
    private JTextField jTextoCodificadoDecodificacao;
    private JTextField jTextoDeEntrada;
    private JLabel jTextoDeEntradaCodificacao;
    private JTextField jTextoDecodificadoDecodificacao;

    public PainelPrincipal()
    {
        passoCorrente = 0;
    }

    public void init()
    {
        initComponents();
        buttonGroup1.add(jRadioButtonExecucaoDiretaCodificacao);
        buttonGroup1.add(jRadioButtonExecucaoPassoAPassoCodificacao);
        buttonGroup2.add(jRadioButtonCodificacao);
        buttonGroup2.add(jRadioButtonDecodificacao);
        buttonGroup3.add(jRadioButtonDiretoDecodificacao);
        buttonGroup3.add(jRadioButtonPassoAPassoDecodificacao);
        jPanel1.setVisible(false);
        jPanel2.setVisible(false);
        jPanel3.setVisible(false);
        jPanel4.setVisible(true);
        jPanel5.setVisible(false);
        jPanel6.setVisible(false);
        jPanel7.setVisible(false);
    }

    private void ordenarVetorFrequenciasDeFormaDecrescente()
    {
        for(int i = 0; i < dadosLinhaTabela.size() - 1; i++)
        {
            int indMaximo = i;
            for(int j = i + 1; j < dadosLinhaTabela.size(); j++)
            {
                if(Double.valueOf((String)((ArrayList)dadosLinhaTabela.get(j)).get(1)).doubleValue() > Double.valueOf((String)((ArrayList)dadosLinhaTabela.get(indMaximo)).get(1)).doubleValue())
                {
                    indMaximo = j;
                }
            }

            if(i != indMaximo)
            {
                String caractere = (String)((ArrayList)dadosLinhaTabela.get(i)).get(0);
                String valor = (String)((ArrayList)dadosLinhaTabela.get(i)).get(1);
                ((ArrayList)dadosLinhaTabela.get(i)).set(0, ((ArrayList)dadosLinhaTabela.get(indMaximo)).get(0));
                ((ArrayList)dadosLinhaTabela.get(i)).set(1, ((ArrayList)dadosLinhaTabela.get(indMaximo)).get(1));
                ((ArrayList)dadosLinhaTabela.get(indMaximo)).set(0, caractere);
                ((ArrayList)dadosLinhaTabela.get(indMaximo)).set(1, valor);
            }
        }

    }

    private void montarTabela(ArrayList vetorFrequenciasNoPasso)
    {
        HashMap listaCaracteres = null;
        String textoUsuario = "";
        ArrayList colunas = new ArrayList();
        colunas.add("Caractere");
        colunas.add("Frequ\352ncia");
        if(vetorFrequenciasNoPasso == null)
        {
            listaCaracteres = new HashMap();
            textoUsuario = jTextoDeEntrada.getText();
            for(int i = 0; i < textoUsuario.length(); i++)
            {
                if(!listaCaracteres.containsKey(Character.valueOf(textoUsuario.charAt(i))))
                {
                    listaCaracteres.put(Character.valueOf(textoUsuario.charAt(i)), Integer.valueOf(1));
                } else
                {
                    Integer qtdAnterior = (Integer)listaCaracteres.get(Character.valueOf(textoUsuario.charAt(i)));
                    listaCaracteres.put(Character.valueOf(textoUsuario.charAt(i)), Integer.valueOf(qtdAnterior.intValue() + 1));
                }
            }

            if(dadosLinhaTabela == null)
            {
                dadosLinhaTabela = new ArrayList();
            } else
            {
                dadosLinhaTabela.clear();
            }
            ArrayList dado;
            for(Iterator i$ = listaCaracteres.entrySet().iterator(); i$.hasNext(); dadosLinhaTabela.add(dado))
            {
                java.util.Map.Entry o = (java.util.Map.Entry)i$.next();
                dado = new ArrayList();
                dado.add(((Character)o.getKey()).toString());
                Integer frequencia = Integer.valueOf(((Integer)o.getValue()).intValue());
                dado.add(frequencia.toString());
            }

            ordenarVetorFrequenciasDeFormaDecrescente();
            OtherTableModel modeloTabela = new OtherTableModel(dadosLinhaTabela, colunas);
            jTabelaDeFrequenciaCodificacaoOriginal.setModel(modeloTabela);
            jTabelaDeFrequenciaCodificacaoCorrente.setModel(modeloTabela);
        } else
        {
            dadosLinhaTabela.clear();
            ArrayList dado;
            for(Iterator i$ = vetorFrequenciasNoPasso.iterator(); i$.hasNext(); dadosLinhaTabela.add(dado))
            {
                Frequencia f = (Frequencia)i$.next();
                dado = new ArrayList();
                dado.add(f.getRotulo());
                dado.add(f.getFrequencia());
            }

            ordenarVetorFrequenciasDeFormaDecrescente();
            OtherTableModel modeloTabela = new OtherTableModel(dadosLinhaTabela, colunas);
            jTabelaDeFrequenciaCodificacaoCorrente.setModel(modeloTabela);
        }
    }

    private void desenharArvore(boolean eUltimoPasso)
    {
        edu.uci.ics.jung.algorithms.layout.Layout layout = new JCustomTreeLayout(gerenciadorFloresta.getFloresta(), 80, 80);
        if(painelComRolagem != null)
        {
            remove(painelComRolagem);
            painelComRolagem.remove(vv);
        }
        vv = new VisualizationViewer(layout);
        vv.setBackground(Color.white);
        vv.setBounds(0, 0, 400, 300);
        vv.setVisible(true);
        painelComRolagem = new GraphZoomScrollPane(vv);
        painelComRolagem.setBounds(350, 70, 900, 475);
        painelComRolagem.setVisible(true);
        vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        vv.getRenderContext().setEdgeFontTransformer(new FontTransformer());
        vv.getRenderContext().setVertexFontTransformer(new FontTransformer());
        vv.getRenderContext().setEdgeLabelClosenessTransformer(new JCustomEdgeLabelClosenessTransformer());
        vv.getRenderContext().setVertexFillPaintTransformer(new JCustomVertexFillPaintTransformer());
        if(eUltimoPasso)
        {
            vv.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller());
        }
        vv.getRenderContext().setVertexShapeTransformer(new JCustomVertexShapeTransformer());
        vv.getRenderer().getVertexLabelRenderer().setPosition(edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position.CNTR);
        if(eUltimoPasso)
        {
            No raiz = null;
            for(Iterator i$ = gerenciadorFloresta.getFloresta().getTrees().iterator(); i$.hasNext();)
            {
                Tree t = (Tree)i$.next();
                raiz = (No)t.getRoot();
            }

            ((JCustomTreeLayout)layout).AjustarPosicoesDosVerticesDaArvore(raiz);
            DefaultModalGraphMouse gm = new DefaultModalGraphMouse();
            gm.setMode(edu.uci.ics.jung.visualization.control.ModalGraphMouse.Mode.TRANSFORMING);
            vv.setGraphMouse(gm);
        }
        add(painelComRolagem, "Center");
        validate();
    }

    private void execucaoDiretaCodificacao()
    {
        montarTabela(null);
        int tamanho = dadosLinhaTabela.size();
        for(int i = 1; i < tamanho; i++)
        {
            gerenciadorFloresta.montarVetorFrequenciaNoPasso(i);
            montarTabela((ArrayList)gerenciadorFloresta.getDadosFrequenciaPorPassos().get(i));
            gerenciadorFloresta.montarFlorestaNoPasso(i);
        }

        desenharArvore(true);
        jGerarCodigoCodificacaoActionPerformed(null);
    }

    private void execucaoDiretaDecodificacao()
    {
        String listaCaracteresCodificados[] = jTextoCodificadoDecodificacao.getText().split(" ");
        String saida = "";
        String arr$[] = listaCaracteresCodificados;
        int len$ = arr$.length;
label0:
        for(int j$ = 0; j$ < len$; j$++)
        {
            String s = arr$[j$];
            Iterator i$ = dadosLinhaTabela.iterator();
            ArrayList v;
            do
            {
                if(!i$.hasNext())
                {
                    continue label0;
                }
                v = (ArrayList)i$.next();
            } while(!((String)v.get(2)).equalsIgnoreCase(s));
            saida = (new StringBuilder()).append(saida).append((String)v.get(0)).toString();
        }

        jTextoDecodificadoDecodificacao.setText(saida);
    }

    private String gerarCodigoCaracterArvore(No no, String rotulo)
    {
        String codigo = "";
        if(no.getNoDireito() != null)
        {
            if(no.getNoDireito().getRotulo().equalsIgnoreCase(rotulo))
            {
                codigo = (new StringBuilder()).append(codigo).append("1").toString();
                return codigo;
            }
            codigo = (new StringBuilder()).append(codigo).append(gerarCodigoCaracterArvore(no.getNoDireito(), rotulo)).toString();
            if(!codigo.isEmpty())
            {
                codigo = (new StringBuilder()).append("1").append(codigo).toString();
                return codigo;
            }
        }
        if(no.getNoEsquerdo() != null)
        {
            if(no.getNoEsquerdo().getRotulo().equalsIgnoreCase(rotulo))
            {
                codigo = (new StringBuilder()).append(codigo).append("0").toString();
                return codigo;
            }
            codigo = (new StringBuilder()).append(codigo).append(gerarCodigoCaracterArvore(no.getNoEsquerdo(), rotulo)).toString();
            if(!codigo.isEmpty())
            {
                codigo = (new StringBuilder()).append("0").append(codigo).toString();
                return codigo;
            }
        }
        return codigo;
    }

    private void montarTabelaHuffman()
    {
        ArrayList vetorDoPrimeiroPasso = (ArrayList)gerenciadorFloresta.getDadosFrequenciaPorPassos().get(0);
        dadosLinhaTabela.clear();
        ArrayList dado;
        for(Iterator i$ = vetorDoPrimeiroPasso.iterator(); i$.hasNext(); dadosLinhaTabela.add(dado))
        {
            Frequencia f = (Frequencia)i$.next();
            dado = new ArrayList();
            dado.add(f.getRotulo());
            dado.add(f.getFrequencia());
            int indUltimoPasso = gerenciadorFloresta.getListaDeArvoresPorPassos().size() - 1;
            String codigo = "";
            codigo = gerarCodigoCaracterArvore((No)((DelegateTree)((ArrayList)gerenciadorFloresta.getListaDeArvoresPorPassos().get(indUltimoPasso)).get(0)).getRoot(), f.getRotulo());
            dado.add(codigo);
        }

        ArrayList colunas = new ArrayList();
        colunas.add("Caractere");
        colunas.add("Frequ\352ncia");
        colunas.add("Codifica\347\343o");
        OtherTableModel modeloTabela = new OtherTableModel(dadosLinhaTabela, colunas);
        jTabelaDeFrequenciaCodificacaoOriginal.setModel(modeloTabela);
    }

    private void construirTabelaDecodificacao(String s)
    {
        if(dadosLinhaTabela != null)
        {
            dadosLinhaTabela.clear();
        } else
        {
            dadosLinhaTabela = new ArrayList();
        }
        String lista[] = s.split(";");
        for(int i = 0; i < lista.length - 2; i += 3)
        {
            ArrayList v = new ArrayList();
            v.add(lista[i]);
            v.add(lista[i + 1]);
            v.add(lista[i + 2]);
            dadosLinhaTabela.add(v);
        }

        ArrayList colunas = new ArrayList();
        colunas.add("Caractere");
        colunas.add("Frequ\352ncia");
        colunas.add("Codifica\347\343o");
        OtherTableModel modeloTabela = new OtherTableModel(dadosLinhaTabela, colunas);
        jTabelaDeFrequenciaDecodificacaoOriginal.setModel(modeloTabela);
    }

    private void initComponents()
    {
        jFrame1 = new JFrame();
        jDialog1 = new JDialog();
        buttonGroup1 = new ButtonGroup();
        buttonGroup2 = new ButtonGroup();
        buttonGroup3 = new ButtonGroup();
        jPanel1 = new JPanel();
        jLabel2 = new JLabel();
        jTextoDeEntrada = new JTextField();
        jRadioButtonExecucaoDiretaCodificacao = new JRadioButton();
        jLabel3 = new JLabel();
        jRadioButtonExecucaoPassoAPassoCodificacao = new JRadioButton();
        jBotaoCodificar = new JButton();
        jButtonVoltarCodificacao = new JButton();
        jLabel1 = new JLabel();
        jPanel2 = new JPanel();
        jLabel4 = new JLabel();
        jScrollPane1 = new JScrollPane();
        jTabelaDeFrequenciaCodificacaoOriginal = new JTable();
        jLabel5 = new JLabel();
        jScrollPane2 = new JScrollPane();
        jTabelaDeFrequenciaCodificacaoCorrente = new JTable();
        jPanel4 = new JPanel();
        jLabel6 = new JLabel();
        jRadioButtonCodificacao = new JRadioButton();
        jRadioButtonDecodificacao = new JRadioButton();
        jBotaoSeguinte = new JButton();
        jPanel5 = new JPanel();
        jLabel8 = new JLabel();
        jTextFieldArquivoImportado = new JTextField();
        jBotaoImportarDecodificacao = new JButton();
        jBotaoDecodificar = new JButton();
        jBotaoVoltarDecodificacaoInicio = new JButton();
        jRadioButtonDiretoDecodificacao = new JRadioButton();
        jRadioButtonPassoAPassoDecodificacao = new JRadioButton();
        jLabel9 = new JLabel();
        jPanel6 = new JPanel();
        jLabel10 = new JLabel();
        jScrollPane3 = new JScrollPane();
        jTabelaDeFrequenciaDecodificacaoOriginal = new JTable();
        jPanel7 = new JPanel();
        jLabelTextoDecodificado = new JLabel();
        jLabel11 = new JLabel();
        jTextoCodificadoDecodificacao = new JTextField();
        jBotaoInicioDecodificacao = new JButton();
        jBotaoAnteriorDecodificacao = new JButton();
        jBotaoProximoDecodificacao = new JButton();
        jTextoDecodificadoDecodificacao = new JTextField();
        jPanel3 = new JPanel();
        jTextoDeEntradaCodificacao = new JLabel();
        jLabel7 = new JLabel();
        jTextoCodificadoCodificacao = new JTextField();
        jBotaoInicioCodificacao = new JButton();
        jBotaoAnteriorCodificacao = new JButton();
        jBotaoProximoCodificacao = new JButton();
        jGerarCodigoCodificacao = new JButton();
        jLabelTaxaCompactacao = new JLabel();
        jButtonExportarCodificacao = new JButton();
        GroupLayout jFrame1Layout = new GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        jFrame1Layout.setVerticalGroup(jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
        GroupLayout jDialog1Layout = new GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        jDialog1Layout.setVerticalGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
        setCursor(new Cursor(0));
        getContentPane().setLayout(null);
        jPanel1.setBorder(BorderFactory.createEtchedBorder());
        jLabel2.setText("Insira o texto:");
        jRadioButtonExecucaoDiretaCodificacao.setText("Direta");
        jRadioButtonExecucaoDiretaCodificacao.setActionCommand("DIRETA");
        jLabel3.setText("Tipo de Execu\347\343o:");
        jRadioButtonExecucaoPassoAPassoCodificacao.setText("Passo a passo");
        jRadioButtonExecucaoPassoAPassoCodificacao.setActionCommand("PASSO_A_PASSO");
        jBotaoCodificar.setText("Codificar");
        jBotaoCodificar.setActionCommand("CODIFICAR");
        jBotaoCodificar.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoCodificarActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        jButtonVoltarCodificacao.setText("Voltar");
        jButtonVoltarCodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jButtonVoltarCodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        GroupLayout jPanel1Layout = new GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addContainerGap().addComponent(jLabel2, -2, 99, -2).addGap(2, 2, 2).addComponent(jTextoDeEntrada, -1, 411, 32767)).addGroup(jPanel1Layout.createSequentialGroup().addGap(12, 12, 12).addComponent(jLabel3)).addGroup(jPanel1Layout.createSequentialGroup().addGap(12, 12, 12).addComponent(jRadioButtonExecucaoDiretaCodificacao).addGap(6, 6, 6).addComponent(jRadioButtonExecucaoPassoAPassoCodificacao)).addGroup(jPanel1Layout.createSequentialGroup().addGap(173, 173, 173).addComponent(jButtonVoltarCodificacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jBotaoCodificar))).addContainerGap()));
        jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel1Layout.createSequentialGroup().addGap(17, 17, 17).addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jLabel2).addComponent(jTextoDeEntrada, -2, -1, -2)).addGap(17, 17, 17).addComponent(jLabel3).addGap(2, 2, 2).addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jRadioButtonExecucaoDiretaCodificacao).addComponent(jRadioButtonExecucaoPassoAPassoCodificacao)).addGap(4, 4, 4).addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jBotaoCodificar).addComponent(jButtonVoltarCodificacao)).addContainerGap()));
        getContentPane().add(jPanel1);
        jPanel1.setBounds(12, 70, 540, 151);
        jLabel1.setFont(new Font("DejaVu Sans", 0, 36));
        jLabel1.setText("Codificador/Decodificador de Huffman");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(40, 10, 720, 54);
        jPanel2.setBorder(BorderFactory.createEtchedBorder());
        jLabel4.setText("Tabela de Frequ\352ncia Original: ");
        jTabelaDeFrequenciaCodificacaoOriginal.setModel(new OtherTableModel(new Object[][] {
            new Object[] {
                null, null
            }, new Object[] {
                null, null
            }, new Object[] {
                null, null
            }, new Object[] {
                null, null
            }
        }, new String[] {
            "Caractere", "Frequ\352ncia"
        }));
        jScrollPane1.setViewportView(jTabelaDeFrequenciaCodificacaoOriginal);
        jLabel5.setText("Tabela de Frequ\352ncia Corrente:");
        jTabelaDeFrequenciaCodificacaoCorrente.setModel(new OtherTableModel(new Object[][] {
            new Object[] {
                null, null
            }, new Object[] {
                null, null
            }, new Object[] {
                null, null
            }, new Object[] {
                null, null
            }
        }, new String[] {
            "Caractere", "Frequ\352ncia"
        }));
        jScrollPane2.setViewportView(jTabelaDeFrequenciaCodificacaoCorrente);
        GroupLayout jPanel2Layout = new GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(jLabel5)).addGroup(jPanel2Layout.createSequentialGroup().addGap(51, 51, 51).addComponent(jScrollPane2, -2, 183, -2)).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(jScrollPane1, -1, 260, 32767)).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(jLabel4, -2, 232, -2))).addContainerGap()));
        jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(jLabel4).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jScrollPane1, -2, 191, -2).addGap(12, 12, 12).addComponent(jLabel5).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jScrollPane2, -2, 192, -2).addContainerGap(19, 32767)));
        getContentPane().add(jPanel2);
        jPanel2.setBounds(12, 70, 288, 476);
        jPanel4.setBorder(BorderFactory.createEtchedBorder());
        jLabel6.setText("Tarefa:");
        jRadioButtonCodificacao.setText("Codifica\347\343o");
        jRadioButtonCodificacao.setActionCommand("DIRETA");
        jRadioButtonDecodificacao.setText("Decodifica\347\343o");
        jRadioButtonDecodificacao.setActionCommand("PASSO_A_PASSO");
        jRadioButtonDecodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jRadioButtonDecodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        jBotaoSeguinte.setText("Pr\363ximo");
        jBotaoSeguinte.setActionCommand("CODIFICAR");
        jBotaoSeguinte.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoSeguinteActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        GroupLayout jPanel4Layout = new GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel4Layout.createSequentialGroup().addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel4Layout.createSequentialGroup().addGap(57, 57, 57).addComponent(jRadioButtonCodificacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jRadioButtonDecodificacao)).addGroup(jPanel4Layout.createSequentialGroup().addContainerGap().addComponent(jLabel6, -2, 243, -2)).addGroup(jPanel4Layout.createSequentialGroup().addGap(142, 142, 142).addComponent(jBotaoSeguinte))).addContainerGap(63, 32767)));
        jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel4Layout.createSequentialGroup().addGap(22, 22, 22).addComponent(jLabel6).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jRadioButtonCodificacao).addComponent(jRadioButtonDecodificacao)).addGap(18, 18, 18).addComponent(jBotaoSeguinte).addGap(21, 21, 21)));
        getContentPane().add(jPanel4);
        jPanel4.setBounds(12, 70, 340, 139);
        jPanel5.setBorder(BorderFactory.createEtchedBorder());
        jLabel8.setText("Arquivo:");
        jBotaoImportarDecodificacao.setText("Importar");
        jBotaoImportarDecodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoImportarDecodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        jBotaoDecodificar.setText("Decodificar");
        jBotaoDecodificar.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoDecodificarActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
               // super();
            }
        });
        jBotaoVoltarDecodificacaoInicio.setText("Voltar");
        jBotaoVoltarDecodificacaoInicio.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoVoltarDecodificacaoInicioActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        jRadioButtonDiretoDecodificacao.setText("Direto");
        jRadioButtonPassoAPassoDecodificacao.setText("Passo a Passo");
        jLabel9.setText("Modo de Execu\347\343o: ");
        GroupLayout jPanel5Layout = new GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel5Layout.createSequentialGroup().addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel5Layout.createSequentialGroup().addContainerGap().addComponent(jRadioButtonDiretoDecodificacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jRadioButtonPassoAPassoDecodificacao)).addGroup(jPanel5Layout.createSequentialGroup().addContainerGap().addComponent(jLabel9)).addGroup(jPanel5Layout.createSequentialGroup().addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING).addGroup(jPanel5Layout.createSequentialGroup().addContainerGap().addComponent(jBotaoVoltarDecodificacaoInicio).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jBotaoDecodificar).addGap(12, 12, 12)).addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel5Layout.createSequentialGroup().addContainerGap().addComponent(jLabel8).addGap(3, 3, 3).addComponent(jTextFieldArquivoImportado, -2, 256, -2))).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jBotaoImportarDecodificacao))).addContainerGap(50, 32767)));
        jPanel5Layout.setVerticalGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel5Layout.createSequentialGroup().addGap(22, 22, 22).addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jLabel8).addComponent(jTextFieldArquivoImportado, -2, -1, -2).addComponent(jBotaoImportarDecodificacao)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jLabel9).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jRadioButtonDiretoDecodificacao).addComponent(jRadioButtonPassoAPassoDecodificacao)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jBotaoVoltarDecodificacaoInicio).addComponent(jBotaoDecodificar)).addGap(546, 546, 546)));
        getContentPane().add(jPanel5);
        jPanel5.setBounds(12, 70, 450, 160);
        jPanel6.setBorder(BorderFactory.createEtchedBorder());
        jLabel10.setText("Tabela de Frequ\352ncia Original: ");
        jTabelaDeFrequenciaDecodificacaoOriginal.setModel(new OtherTableModel(new Object[][] {
            new Object[] {
                null, null, null
            }, new Object[] {
                null, null, null
            }, new Object[] {
                null, null, null
            }, new Object[] {
                null, null, null
            }
        }, new String[] {
            "Caractere", "Frequ\352ncia", "Codifica\347\343o"
        }) {

            boolean canEdit[] = {
                false, false, false
            };
            final PainelPrincipal this$0;

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super(x0, x1);
            }
        });
        jScrollPane3.setViewportView(jTabelaDeFrequenciaDecodificacaoOriginal);
        GroupLayout jPanel6Layout = new GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel6Layout.createSequentialGroup().addContainerGap().addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addComponent(jLabel10, -2, 232, -2).addComponent(jScrollPane3, -1, 260, 32767)).addContainerGap()));
        jPanel6Layout.setVerticalGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel6Layout.createSequentialGroup().addContainerGap().addComponent(jLabel10).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jScrollPane3, -1, 425, 32767).addContainerGap()));
        getContentPane().add(jPanel6);
        jPanel6.setBounds(12, 70, 288, 476);
        jPanel7.setBorder(BorderFactory.createEtchedBorder());
        jLabelTextoDecodificado.setText("Texto Decodificado:");
        jLabel11.setText("Texto Codificado:");
        jBotaoInicioDecodificacao.setText("In\355cio");
        jBotaoInicioDecodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoInicioDecodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        jBotaoAnteriorDecodificacao.setText("Anterior");
        jBotaoAnteriorDecodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoAnteriorDecodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        jBotaoProximoDecodificacao.setText("Pr\363ximo");
        jBotaoProximoDecodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoProximoDecodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
               // super();
            }
        });
        GroupLayout jPanel7Layout = new GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel7Layout.createSequentialGroup().addContainerGap().addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel7Layout.createSequentialGroup().addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel7Layout.createSequentialGroup().addComponent(jLabel11).addGap(3, 3, 3).addComponent(jTextoCodificadoDecodificacao, -1, 523, 32767)).addGroup(jPanel7Layout.createSequentialGroup().addComponent(jLabelTextoDecodificado).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent(jTextoDecodificadoDecodificacao, -2, 498, -2))).addGap(33, 33, 33)).addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup().addComponent(jBotaoInicioDecodificacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jBotaoAnteriorDecodificacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jBotaoProximoDecodificacao).addGap(238, 238, 238)))));
        jPanel7Layout.setVerticalGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup().addContainerGap().addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jLabelTextoDecodificado).addComponent(jTextoDecodificadoDecodificacao, -2, -1, -2)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jLabel11).addComponent(jTextoCodificadoDecodificacao, -2, -1, -2)).addGap(10, 10, 10).addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jBotaoAnteriorDecodificacao).addComponent(jBotaoInicioDecodificacao).addComponent(jBotaoProximoDecodificacao)).addContainerGap()));
        getContentPane().add(jPanel7);
        jPanel7.setBounds(12, 550, 680, 120);
        jPanel3.setBorder(BorderFactory.createEtchedBorder());
        jTextoDeEntradaCodificacao.setText("Texto de Entrada: ");
        jLabel7.setText("Texto Codificado:");
        jBotaoInicioCodificacao.setText("In\355cio");
        jBotaoInicioCodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoInicioCodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
               // super();
            }
        });
        jBotaoAnteriorCodificacao.setText("Anterior");
        jBotaoAnteriorCodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoAnteriorCodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        jBotaoProximoCodificacao.setText("Pr\363ximo");
        jBotaoProximoCodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jBotaoProximoCodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        jGerarCodigoCodificacao.setText("Gerar C\363digo");
        jGerarCodigoCodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jGerarCodigoCodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
               // super();
            }
        });
        jLabelTaxaCompactacao.setText("Taxa de Compacta\347\343o:");
        jButtonExportarCodificacao.setText("Exportar");
        jButtonExportarCodificacao.addActionListener(new ActionListener() {

            final PainelPrincipal this$0;

            public void actionPerformed(ActionEvent evt)
            {
                jButtonExportarCodificacaoActionPerformed(evt);
            }

            
            {
                this$0 = PainelPrincipal.this;
                //super();
            }
        });
        GroupLayout jPanel3Layout = new GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel3Layout.createSequentialGroup().addContainerGap().addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel3Layout.createSequentialGroup().addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(jPanel3Layout.createSequentialGroup().addComponent(jLabel7).addGap(4, 4, 4).addComponent(jTextoCodificadoCodificacao, -2, 573, -2)).addGroup(jPanel3Layout.createSequentialGroup().addGap(133, 133, 133).addComponent(jBotaoInicioCodificacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jBotaoAnteriorCodificacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jBotaoProximoCodificacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(jGerarCodigoCodificacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED).addComponent(jButtonExportarCodificacao))).addContainerGap(38, 32767)).addGroup(jPanel3Layout.createSequentialGroup().addComponent(jLabelTaxaCompactacao, -1, -1, 32767).addGap(577, 577, 577)).addGroup(jPanel3Layout.createSequentialGroup().addComponent(jTextoDeEntradaCodificacao).addContainerGap(607, 32767)))));
        jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup().addGap(10, 10, 10).addComponent(jLabelTaxaCompactacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addComponent(jTextoDeEntradaCodificacao).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jLabel7).addComponent(jTextoCodificadoCodificacao, -2, -1, -2)).addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED).addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE).addComponent(jBotaoInicioCodificacao).addComponent(jBotaoAnteriorCodificacao).addComponent(jBotaoProximoCodificacao).addComponent(jGerarCodigoCodificacao).addComponent(jButtonExportarCodificacao)).addContainerGap()));
        getContentPane().add(jPanel3);
        jPanel3.setBounds(10, 550, 740, 130);
    }

    private void jBotaoCodificarActionPerformed(ActionEvent evt)
    {
        if(!jTextoDeEntrada.getText().isEmpty() && (jRadioButtonExecucaoPassoAPassoCodificacao.isSelected() || jRadioButtonExecucaoDiretaCodificacao.isSelected()))
        {
            montarTabela(null);
            gerenciadorFloresta = new GerenciadorFloresta(dadosLinhaTabela);
            gerenciadorFloresta.montarFlorestaNoPasso(passoCorrente);
            desenharArvore(false);
            if(jRadioButtonExecucaoPassoAPassoCodificacao.isSelected())
            {
                jPanel1.setVisible(false);
                jPanel2.setVisible(true);
                jPanel3.setVisible(true);
                jTabelaDeFrequenciaCodificacaoCorrente.setVisible(true);
                jTextoDeEntradaCodificacao.setText((new StringBuilder()).append("Entrada de Texto: ").append(jTextoDeEntrada.getText()).toString());
                jTextoCodificadoCodificacao.setEditable(false);
                jBotaoAnteriorCodificacao.setEnabled(false);
                jBotaoProximoCodificacao.setEnabled(true);
                jTextoCodificadoCodificacao.setText("");
                jLabel7.setVisible(false);
                jLabelTaxaCompactacao.setVisible(false);
                jTextoCodificadoCodificacao.setVisible(false);
                jGerarCodigoCodificacao.setVisible(false);
                jButtonExportarCodificacao.setVisible(false);
            } else
            if(jRadioButtonExecucaoDiretaCodificacao.isSelected())
            {
                jTabelaDeFrequenciaCodificacaoCorrente.setVisible(false);
                jPanel1.setVisible(false);
                jPanel2.setVisible(true);
                jPanel3.setVisible(true);
                jTextoDeEntradaCodificacao.setText((new StringBuilder()).append("Entrada de Texto: ").append(jTextoDeEntrada.getText()).toString());
                jTextoCodificadoCodificacao.setEditable(false);
                jLabelTaxaCompactacao.setVisible(false);
                jBotaoAnteriorCodificacao.setEnabled(false);
                jBotaoProximoCodificacao.setEnabled(false);
                jLabel7.setVisible(false);
                jTextoCodificadoCodificacao.setVisible(false);
                jGerarCodigoCodificacao.setVisible(false);
                jButtonExportarCodificacao.setVisible(true);
                execucaoDiretaCodificacao();
            }
        } else
        {
            JOptionPane.showMessageDialog(this, "Voc\352 deve digitar o texto que ser\341 codificado e escolher a forma de execu\347" +
"\343o para prosseguir."
, "Informa\347\343o", -1);
        }
    }

    private void jBotaoProximoCodificacaoActionPerformed(ActionEvent evt)
    {
        jBotaoAnteriorCodificacao.setEnabled(true);
        jButtonExportarCodificacao.setVisible(false);
        boolean eUltimoPasso = false;
        if(((ArrayList)gerenciadorFloresta.getDadosFrequenciaPorPassos().get(passoCorrente)).size() == 2)
        {
            jBotaoProximoCodificacao.setEnabled(false);
            jGerarCodigoCodificacao.setVisible(true);
            eUltimoPasso = true;
        }
        passoCorrente++;
        gerenciadorFloresta.montarVetorFrequenciaNoPasso(passoCorrente);
        montarTabela((ArrayList)gerenciadorFloresta.getDadosFrequenciaPorPassos().get(passoCorrente));
        gerenciadorFloresta.montarFlorestaNoPasso(passoCorrente);
        System.out.println((new StringBuilder()).append("Passo:").append(passoCorrente).toString());
        Tree a;
        for(Iterator i$ = gerenciadorFloresta.getFloresta().getTrees().iterator(); i$.hasNext(); System.out.println(a.toString()))
        {
            a = (Tree)i$.next();
            System.out.println((new StringBuilder()).append("Raiz:").append(a.getRoot()).toString());
        }

        desenharArvore(eUltimoPasso);
    }

    private void jBotaoInicioCodificacaoActionPerformed(ActionEvent evt)
    {
        jPanel1.setVisible(true);
        jTextoDeEntrada.setText("");
        jRadioButtonExecucaoDiretaCodificacao.setSelected(false);
        jRadioButtonExecucaoPassoAPassoCodificacao.setSelected(false);
        jPanel2.setVisible(false);
        jTextoDeEntradaCodificacao.setText("Texto de Entrada: ");
        jTextoCodificadoCodificacao.setText("");
        jPanel3.setVisible(false);
        remove(painelComRolagem);
        passoCorrente = 0;
        repaint();
    }

    private void jBotaoAnteriorCodificacaoActionPerformed(ActionEvent evt)
    {
        jTabelaDeFrequenciaCodificacaoCorrente.setVisible(true);
        jBotaoProximoCodificacao.setEnabled(true);
        jButtonExportarCodificacao.setVisible(false);
        jGerarCodigoCodificacao.setVisible(false);
        jTextoCodificadoCodificacao.setVisible(false);
        jTextoCodificadoCodificacao.setText("");
        jLabel7.setVisible(false);
        jLabelTaxaCompactacao.setVisible(false);
        passoCorrente--;
        gerenciadorFloresta.montarFlorestaNoPasso(passoCorrente);
        montarTabela((ArrayList)gerenciadorFloresta.getDadosFrequenciaPorPassos().get(passoCorrente));
        desenharArvore(false);
        if(passoCorrente - 1 < 0)
        {
            jBotaoAnteriorCodificacao.setEnabled(false);
        }
    }

    private void jGerarCodigoCodificacaoActionPerformed(ActionEvent evt)
    {
        jTabelaDeFrequenciaCodificacaoCorrente.setVisible(false);
        jTextoCodificadoCodificacao.setVisible(true);
        jButtonExportarCodificacao.setVisible(true);
        jLabel7.setVisible(true);
        jGerarCodigoCodificacao.setVisible(false);
        montarTabelaHuffman();
        String saida = "";
        int numeroBitsStringCodificada = 0;
        for(int i = 0; i < jTextoDeEntrada.getText().length(); i++)
        {
            String caracter = String.valueOf(jTextoDeEntrada.getText().charAt(i));
            int j = 0;
            do
            {
                if(j >= dadosLinhaTabela.size())
                {
                    break;
                }
                if(caracter.equalsIgnoreCase((String)((ArrayList)dadosLinhaTabela.get(j)).get(0)))
                {
                    saida = (new StringBuilder()).append(saida).append((String)((ArrayList)dadosLinhaTabela.get(j)).get(2)).toString();
                    numeroBitsStringCodificada += ((String)((ArrayList)dadosLinhaTabela.get(j)).get(2)).length();
                    break;
                }
                j++;
            } while(true);
            saida = (new StringBuilder()).append(saida).append(" ").toString();
        }

        jTextoCodificadoCodificacao.setText(saida);
        jLabelTaxaCompactacao.setVisible(true);
        int numeroBitsStringEntrada = 8 * jTextoDeEntrada.getText().length();
        String taxaSaida = (new StringBuilder()).append("Taxa de Compacta\347\343o: 8 bits x  ").append(String.valueOf(jTextoDeEntrada.getText().length())).append(" : 1 bit x ").append(String.valueOf(numeroBitsStringCodificada)).toString();
        taxaSaida = (new StringBuilder()).append(taxaSaida).append(" = ").append(String.valueOf(numeroBitsStringEntrada)).append(":").append(String.valueOf(numeroBitsStringCodificada)).toString();
        jLabelTaxaCompactacao.setText(taxaSaida);
        repaint();
    }

    private void jRadioButtonDecodificacaoActionPerformed(ActionEvent actionevent)
    {
    }

    private void jBotaoSeguinteActionPerformed(ActionEvent evt)
    {
        if(jRadioButtonCodificacao.isSelected() || jRadioButtonDecodificacao.isSelected())
        {
            if(jRadioButtonCodificacao.isSelected())
            {
                jPanel1.setVisible(true);
            } else
            {
                jPanel5.setVisible(true);
                jBotaoDecodificar.setVisible(false);
            }
            jPanel4.setVisible(false);
        } else
        {
            JOptionPane.showMessageDialog(this, "Voc\352 deve escolher uma das op\347\365es para prosseguir.", "Informa\347\343o", -1);
        }
    }

    private void jBotaoImportarDecodificacaoActionPerformed(ActionEvent evt)
    {
        JFrame frame = new JFrame();
        JFileChooser jfc = new JFileChooser();
        int resultado = jfc.showOpenDialog(frame);
        if(resultado == 1)
        {
            jBotaoDecodificar.setVisible(false);
            return;
        }
        File arquivo = jfc.getSelectedFile();
        jTextFieldArquivoImportado.setText(arquivo.getAbsolutePath());
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(arquivo));
            String s = "";
            for(int c = 0; (c = br.read()) != -1;)
            {
                s = (new StringBuilder()).append(s).append((char)c).toString();
            }

            br.close();
            construirTabelaDecodificacao(s);
            String lista[] = s.split(";");
            jTextoCodificadoDecodificacao.setText(lista[lista.length - 1]);
            JOptionPane.showMessageDialog(this, "Importa\347\343o Terminada.", "Informa\347\343o", -1);
            jBotaoDecodificar.setVisible(true);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Erro de arquivo", 0);
            jBotaoDecodificar.setVisible(false);
        }
    }

    private void jBotaoDecodificarActionPerformed(ActionEvent evt)
    {
        passoCorrente = 0;
        if(jRadioButtonPassoAPassoDecodificacao.isSelected() || jRadioButtonDiretoDecodificacao.isSelected())
        {
            if(jRadioButtonPassoAPassoDecodificacao.isSelected())
            {
                jPanel5.setVisible(false);
                jPanel6.setVisible(true);
                jPanel7.setVisible(true);
                jTextoDecodificadoDecodificacao.setEditable(false);
                jTextoCodificadoDecodificacao.setEditable(false);
                jBotaoAnteriorDecodificacao.setEnabled(false);
                jBotaoProximoDecodificacao.setEnabled(true);
                jBotaoInicioDecodificacao.setEnabled(true);
                jTextoDecodificadoDecodificacao.setText("");
            } else
            if(jRadioButtonDiretoDecodificacao.isSelected())
            {
                jTabelaDeFrequenciaCodificacaoCorrente.setVisible(false);
                jPanel5.setVisible(false);
                jPanel6.setVisible(true);
                jPanel7.setVisible(true);
                jTextoDecodificadoDecodificacao.setEditable(false);
                jTextoCodificadoDecodificacao.setEditable(false);
                jBotaoAnteriorDecodificacao.setEnabled(false);
                jBotaoProximoDecodificacao.setEnabled(false);
                jBotaoInicioDecodificacao.setEnabled(true);
                execucaoDiretaDecodificacao();
            }
        } else
        {
            JOptionPane.showMessageDialog(this, "Voc\352 deve escolher uma forma de execu\347\343o para prosseguir.", "Informa\347\343o", -1);
        }
    }

    private void jBotaoVoltarDecodificacaoInicioActionPerformed(ActionEvent evt)
    {
        jPanel5.setVisible(false);
        jTextFieldArquivoImportado.setText("");
        jRadioButtonDiretoDecodificacao.setSelected(false);
        jRadioButtonPassoAPassoDecodificacao.setSelected(false);
        jPanel4.setVisible(true);
        jRadioButtonCodificacao.setSelected(false);
        jRadioButtonDecodificacao.setSelected(false);
    }

    private void jButtonVoltarCodificacaoActionPerformed(ActionEvent evt)
    {
        jPanel1.setVisible(false);
        jTextoDeEntrada.setText("");
        jRadioButtonExecucaoPassoAPassoCodificacao.setSelected(false);
        jRadioButtonExecucaoDiretaCodificacao.setSelected(false);
        jPanel4.setVisible(true);
        jRadioButtonCodificacao.setSelected(false);
        jRadioButtonDecodificacao.setSelected(false);
    }

    private void jButtonExportarCodificacaoActionPerformed(ActionEvent evt)
    {
        JFrame frame = new JFrame();
        String caminho = (String)JOptionPane.showInputDialog(frame, "Digite o caminho do arquivo:\n Exemplo: /home/jose/resultadosHuffman.txt", "Caminho do Arquivo", -1, null, null, "");
        File arquivo = new File(caminho);
        try
        {
            BufferedWriter bw = new BufferedWriter(new FileWriter(arquivo));
            ArrayList v;
            for(Iterator i$ = dadosLinhaTabela.iterator(); i$.hasNext(); bw.write((new StringBuilder()).append((String)v.get(2)).append(";").toString()))
            {
                v = (ArrayList)i$.next();
                bw.write((new StringBuilder()).append((String)v.get(0)).append(";").toString());
                bw.write((new StringBuilder()).append((String)v.get(1)).append(";").toString());
            }

            bw.write((new StringBuilder()).append(jTextoCodificadoCodificacao.getText()).append(";").toString());
            bw.close();
            JOptionPane.showMessageDialog(this, "Exporta\347\343o Terminada.", "Informa\347\343o", -1);
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(this, e.getMessage(), "Erro de arquivo", 0);
        }
    }

    private void jBotaoInicioDecodificacaoActionPerformed(ActionEvent evt)
    {
        passoCorrente = 0;
        jPanel6.setVisible(false);
        jPanel7.setVisible(false);
        jTextoDecodificadoDecodificacao.setText("");
        jTextoCodificadoDecodificacao.setText("");
        jPanel5.setVisible(true);
        jTextFieldArquivoImportado.setText("");
        jBotaoDecodificar.setVisible(false);
    }

    private void jBotaoAnteriorDecodificacaoActionPerformed(ActionEvent evt)
    {
        passoCorrente--;
        jBotaoProximoDecodificacao.setEnabled(true);
        if(passoCorrente > 0)
        {
            String novaString = jTextoDecodificadoDecodificacao.getText().substring(0, passoCorrente);
            jTextoDecodificadoDecodificacao.setText(novaString);
        } else
        if(passoCorrente == 0)
        {
            jTextoDecodificadoDecodificacao.setText("");
            jBotaoAnteriorDecodificacao.setEnabled(false);
        }
    }

    private void jBotaoProximoDecodificacaoActionPerformed(ActionEvent evt)
    {
        jBotaoAnteriorDecodificacao.setEnabled(true);
        String listaCaracteresCodificados[] = jTextoCodificadoDecodificacao.getText().split(" ");
        if(passoCorrente + 1 < listaCaracteresCodificados.length)
        {
            Iterator i$ = dadosLinhaTabela.iterator();
            do
            {
                if(!i$.hasNext())
                {
                    break;
                }
                ArrayList v = (ArrayList)i$.next();
                if(!((String)v.get(2)).equalsIgnoreCase(listaCaracteresCodificados[passoCorrente]))
                {
                    continue;
                }
                jTextoDecodificadoDecodificacao.setText((new StringBuilder()).append(jTextoDecodificadoDecodificacao.getText()).append((String)v.get(0)).toString());
                break;
            } while(true);
            passoCorrente++;
        } else
        {
            Iterator i$ = dadosLinhaTabela.iterator();
            do
            {
                if(!i$.hasNext())
                {
                    break;
                }
                ArrayList v = (ArrayList)i$.next();
                if(!((String)v.get(2)).equalsIgnoreCase(listaCaracteresCodificados[passoCorrente]))
                {
                    continue;
                }
                jTextoDecodificadoDecodificacao.setText((new StringBuilder()).append(jTextoDecodificadoDecodificacao.getText()).append((String)v.get(0)).toString());
                break;
            } while(true);
            jBotaoProximoDecodificacao.setEnabled(false);
        }
    }















}
