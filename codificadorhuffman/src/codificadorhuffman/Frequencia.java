package codificadorhuffman;

public class Frequencia
{
  private String rotulo;
  private String frequencia;
  
  public Frequencia() {}
  
  public Frequencia(String rotulo, String frequencia)
  {
    this.rotulo = rotulo;
    this.frequencia = frequencia;
  }
  
  public String getRotulo()
  {
    return this.rotulo;
  }
  
  public void setRotulo(String rotulo)
  {
    this.rotulo = rotulo;
  }
  
  public String getFrequencia()
  {
    return this.frequencia;
  }
  
  public void setFrequencia(String frequencia)
  {
    this.frequencia = frequencia;
  }
  
  public boolean equals(Object obj)
  {
    boolean isEqual = false;
    if ((this.rotulo.equalsIgnoreCase(((Frequencia)obj).getRotulo())) && (this.frequencia.equalsIgnoreCase(((Frequencia)obj).getFrequencia()))) {
      isEqual = true;
    }
    return isEqual;
  }
}
