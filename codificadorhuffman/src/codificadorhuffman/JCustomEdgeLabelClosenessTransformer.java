package codificadorhuffman;

import org.apache.commons.collections15.Transformer;

class JCustomEdgeLabelClosenessTransformer
  implements Transformer
{
  public Number transform(Object i)
  {
    return new Float(0.5D);
  }
}
